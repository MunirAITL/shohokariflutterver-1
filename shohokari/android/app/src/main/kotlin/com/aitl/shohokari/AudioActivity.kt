package com.aitl.shohokari

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import com.shohokari.R
import io.flutter.plugin.common.MethodCall
import java.lang.Exception

class AudioActivity : MediaPlayer.OnPreparedListener {

    var player = MediaPlayer()

    companion object {
        internal val TAG = "AudioActivity"
    }

    fun recommendationsData(callback: (String) -> Unit) {
        callback("munir audio testing")
    }

    public fun start(context: Context, call: MethodCall) {

        stop()

        var file = call.argument<String>("file")
        when (file) {
            "add_task_alert_setting" -> player = MediaPlayer.create(context, R.raw.add_task_alert_setting)
            "browse_task" -> player = MediaPlayer.create(context, R.raw.browse_task)
            "create_profile" -> player = MediaPlayer.create(context, R.raw.create_profile)
            "delete_task_prompt" -> player = MediaPlayer.create(context, R.raw.delete_task_prompt)
            "login" -> player = MediaPlayer.create(context, R.raw.login)
            "make_offer" -> player = MediaPlayer.create(context, R.raw.make_offer)
            "more_dashboard" -> player = MediaPlayer.create(context, R.raw.more_dashboard)
            "more_help" -> player = MediaPlayer.create(context, R.raw.more_help)
            "more_notifications" -> player = MediaPlayer.create(context, R.raw.more_notifications)
            "more_payment_history" -> player = MediaPlayer.create(context, R.raw.more_payment_history)
            "more_payment_method" -> player = MediaPlayer.create(context, R.raw.more_payment_method)
            "more_profile" -> player = MediaPlayer.create(context, R.raw.more_profile)
            "more_setting" -> player = MediaPlayer.create(context, R.raw.more_setting)
            "more_task_alerts" -> player = MediaPlayer.create(context, R.raw.more_task_alerts)
            "my_task" -> player = MediaPlayer.create(context, R.raw.my_task)
            "private_message" -> player = MediaPlayer.create(context, R.raw.private_message)
            "review_offers" -> player = MediaPlayer.create(context, R.raw.review_offers)
            "setting_edit_account" -> player = MediaPlayer.create(context, R.raw.setting_edit_account)
            "setting_notification_settings" -> player = MediaPlayer.create(context, R.raw.setting_notification_settings)
            "task_attachment" -> player = MediaPlayer.create(context, R.raw.task_attachment)
            "task_details_offer" -> player = MediaPlayer.create(context, R.raw.task_details_offer)
            "task_post_budget" -> player = MediaPlayer.create(context, R.raw.task_post_budget)
            "task_post_category" -> player = MediaPlayer.create(context, R.raw.task_post_category)
            "task_post_due_date" -> player = MediaPlayer.create(context, R.raw.task_post_due_date)
            "task_post_title" -> player = MediaPlayer.create(context, R.raw.task_post_title)
            "welcome_shohokari" -> player = MediaPlayer.create(context, R.raw.welcome_shohokari)
            else -> { // Note the block
                print("audio not found : "+file)
            }
        }

        //player.setOnPreparedListener(this);
        //player.prepareAsync();
        player.start()
    }

    override fun onPrepared(player: MediaPlayer) {
        player.start()
    }

    public fun stop() {
        //if (player?.isPlaying == true) {
            //player.reset();
        try {
            if (player.isPlaying)
                player.stop()
        }catch (e:Exception){}
            //player.release()
        //}
    }
}