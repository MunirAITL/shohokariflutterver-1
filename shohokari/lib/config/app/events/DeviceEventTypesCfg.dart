class DeviceEventTypesCfg {
  static const USERDEVICE_EVENTTYPE_INSTALL = "Install";
  static const USERDEVICE_EVENTTYPE_LOGIN = "Login";
  static const USERDEVICE_EVENTTYPE_LOGOUT = "Logout";
  static const USERDEVICE_EVENTTYPE_UnInstall = "UnInstall";

  static const USERDEVICE_EVENTTYPE_VISIT_TASK_CATEGORY_START =
      "Task Category Start";
  static const USERDEVICE_EVENTTYPE_VISIT_TASK_CATEGORY_END =
      "Task Category End";
  static const USERDEVICE_EVENTTYPE_VISIT_BROWSE_START_TASK =
      "Browse Task Start";
  static const USERDEVICE_EVENTTYPE_VISIT_BROWSE_END_TASK = "Browse Task End";

  static const USERDEVICE_EVENTTYPE_VISIT_MYTASK_START = "My Task Start";
  static const USERDEVICE_EVENTTYPE_VISIT_MYTASK_END = "My Task End";

  static const USERDEVICE_EVENTTYPE_VISIT_DASHBOARD_START =
      "Task Dashboard Start";
  static const USERDEVICE_EVENTTYPE_VISIT_DASHBOARD_END = "Task Dashboard End";

  static const USERDEVICE_EVENTTYPE_VISIT_PROFILE_START = "Task Profile Start";
  static const USERDEVICE_EVENTTYPE_VISIT_PROFILE_END = "Task Profile End";

  static const USERDEVICE_EVENTTYPE_VISIT_EDIT_PROFILE_START =
      "Task Edit Profile Start";
  static const USERDEVICE_EVENTTYPE_VISIT_EDIT_PROFILE_END =
      "Task Edit Profile End";

  static const USERDEVICE_EVENTTYPE_VISIT_MORE_START = "More Start";
  static const USERDEVICE_EVENTTYPE_VISIT_MORE_END = "More End";
}
