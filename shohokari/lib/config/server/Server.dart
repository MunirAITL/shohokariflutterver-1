class Server {
  static const bool isTest = true;
  static const bool isOtp = false;

  //  BaseUrl
  static const String BASE_URL =
      (isTest) ? "https://shohokari.com" : "https://shohokari.com";

  static const String WSAPIKEY = "ABCXYZ123TEST";
}
