import 'package:aitl/config/server/Server.dart';

class APIEmailNotiCfg {
  //  step 4
  static const String TASK_EMAI_NOTI_GET_URL =
      Server.BASE_URL + "/api/task/sendemailandnotification/#taskId#";

  static const String COMMENTS_EMAI_NOTI_GET_URL =
      Server.BASE_URL + "/api/comments/sendemailandnotification/#commentId#";

  static const String TIMELINE_EMAI_NOTI_GET_URL =
      Server.BASE_URL + "/api/timeline/sendemailandnotification/#timelineId#";

  static const String TASKBIDDING_EMAI_NOTI_GET_URL = Server.BASE_URL +
      "/api/taskbidding/sendemailandnotification/#taskBiddingId#";

  static const String TASK_USER_RATING_URL = Server.BASE_URL +
      "/api/userrating/sendemailandnotification/#userRatingId#";
}
