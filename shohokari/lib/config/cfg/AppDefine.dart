class AppDefine {
  //  DEFINE STUFF START HERE...
  static const APP_NAME = "Shohokari";
  static const SUPPORT_EMAIL = "support@shohokari.com";
  static const SUPPORT_CALL1 = "096 12 666 666";
  static const SUPPORT_CALL2 = "017 9696 9696";
  static const COUNTRY_DIALCODE = "+880";
  static const COUNTRY_NAME = "Bangladesh";
  static const CUR_SIGN = "৳";
  static const CUR_ABBR = "BDT";
  static const GEO_CODE = "bd";
  static const COUNTRY_FLAG = "bd";
  static const LANGUAGE = "bn";

  static const ORDINAL_NOS = [
    '1st',
    '2nd',
    '3rd',
    '4th',
    '5th',
    '6th',
    '7th',
    '8th',
    '9th',
    '10th',
    '11th',
    '12th',
    '13th',
    '14th',
    '15th',
    '16th',
    '17th',
    '18th',
    '19th',
    '20th',
    '21st',
    '22nd',
    '23rd',
    '24th',
    '25th',
    '26th',
    '27th',
    '28th',
    '29th',
    '30th',
    '31st'
  ];
}
