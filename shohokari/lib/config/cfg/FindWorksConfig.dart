class FindWorksConfig {
  static const int distance = 500;
  static const int minPrice = 20;
  static const int maxPrice = 100000;
  static const double lat = 51.525315960247724;
  static const double lng = -0.13184578844974512;
}
