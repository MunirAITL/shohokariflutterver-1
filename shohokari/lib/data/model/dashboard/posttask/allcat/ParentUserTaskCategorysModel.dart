class ParentUserTaskCategorysModel {
  bool isApp;
  bool isWeb;
  String name;
  String nameBn;
  int order;
  int parentId;
  String remarks;
  int userId;
  int id;

  ParentUserTaskCategorysModel(
      {this.isApp,
      this.isWeb,
      this.name,
      this.nameBn,
      this.order,
      this.parentId,
      this.remarks,
      this.userId,
      this.id});

  ParentUserTaskCategorysModel.fromJson(Map<String, dynamic> json) {
    isApp = json['IsApp'] ?? false;
    isWeb = json['IsWeb'] ?? false;
    name = json['Name'] ?? '';
    nameBn = json['Name_Bn'] ?? '';
    order = json['Order'] ?? 0;
    parentId = json['ParentId'] ?? 0;
    remarks = json['Remarks'] ?? '';
    userId = json['UserId'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IsApp'] = this.isApp;
    data['IsWeb'] = this.isWeb;
    data['Name'] = this.name;
    data['Name_Bn'] = this.nameBn;
    data['Order'] = this.order;
    data['ParentId'] = this.parentId;
    data['Remarks'] = this.remarks;
    data['UserId'] = this.userId;
    data['Id'] = this.id;
    return data;
  }
}
