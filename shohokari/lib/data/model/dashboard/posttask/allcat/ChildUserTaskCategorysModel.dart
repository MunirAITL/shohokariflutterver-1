class ChildUserTaskCategorysModel {
  bool isApp;
  bool isWeb;
  String name;
  String nameBn;
  int order;
  int parentId;
  String remarks;
  int userId;
  int id;
  List<UserTaskItemResponseModelList> userTaskItemResponseModelList;

  ChildUserTaskCategorysModel(
      {this.isApp,
      this.isWeb,
      this.name,
      this.nameBn,
      this.order,
      this.parentId,
      this.remarks,
      this.userId,
      this.id,
      this.userTaskItemResponseModelList});

  ChildUserTaskCategorysModel.fromJson(Map<String, dynamic> json) {
    isApp = json['IsApp'] ?? false;
    isWeb = json['IsWeb'] ?? false;
    name = json['Name'] ?? '';
    nameBn = json['Name_Bn'] ?? '';
    order = json['Order'] ?? 0;
    parentId = json['ParentId'] ?? 0;
    remarks = json['Remarks'] ?? '';
    userId = json['UserId'] ?? 0;
    id = json['Id'] ?? 0;
    if (json['UserTaskItemResponseModelList'] != null) {
      userTaskItemResponseModelList = [];
      json['UserTaskItemResponseModelList'].forEach((v) {
        try {
          userTaskItemResponseModelList
              .add(new UserTaskItemResponseModelList.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IsApp'] = this.isApp;
    data['IsWeb'] = this.isWeb;
    data['Name'] = this.name;
    data['Name_Bn'] = this.nameBn;
    data['Order'] = this.order;
    data['ParentId'] = this.parentId;
    data['Remarks'] = this.remarks;
    data['UserId'] = this.userId;
    data['Id'] = this.id;
    if (this.userTaskItemResponseModelList != null) {
      data['UserTaskItemResponseModelList'] =
          this.userTaskItemResponseModelList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserTaskItemResponseModelList {
  String imageUrl;
  bool isTaskItemForm;
  String name;
  String nameBn;
  int order;
  String pageDescription;
  String pageDescriptionBn;
  String pageTitle;
  String pageTitleBn;
  int tabNumber;
  int userId;
  String userTaskCategoryName;
  int id;
  int userTaskParentCategoryId;
  String remarks;

  UserTaskItemResponseModelList(
      {this.imageUrl,
      this.isTaskItemForm,
      this.name,
      this.nameBn,
      this.order,
      this.pageDescription,
      this.pageDescriptionBn,
      this.pageTitle,
      this.pageTitleBn,
      this.tabNumber,
      this.userId,
      this.userTaskCategoryName,
      this.id,
      this.userTaskParentCategoryId,
      this.remarks});

  UserTaskItemResponseModelList.fromJson(Map<String, dynamic> json) {
    imageUrl = json['ImageUrl'] ?? '';
    isTaskItemForm = json['IsTaskItemForm'] ?? false;
    name = json['Name'] ?? '';
    nameBn = json['Name_Bn'] ?? '';
    order = json['Order'] ?? 0;
    pageDescription = json['PageDescription'] ?? '';
    pageDescriptionBn = json['PageDescription_Bn'] ?? '';
    pageTitle = json['PageTitle'] ?? '';
    pageTitleBn = json['PageTitle_Bn'] ?? '';
    tabNumber = json['TabNumber'] ?? 0;
    userId = json['UserId'] ?? 0;
    userTaskCategoryName = json['UserTaskCategoryName'] ?? '';
    id = json['Id'] ?? 0;
    userTaskParentCategoryId = json['UserTaskParentCategoryId'] ?? 0;
    remarks = json['Remarks'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ImageUrl'] = this.imageUrl;
    data['IsTaskItemForm'] = this.isTaskItemForm;
    data['Name'] = this.name;
    data['Name_Bn'] = this.nameBn;
    data['Order'] = this.order;
    data['PageDescription'] = this.pageDescription;
    data['PageDescription_Bn'] = this.pageDescriptionBn;
    data['PageTitle'] = this.pageTitle;
    data['PageTitle_Bn'] = this.pageTitleBn;
    data['TabNumber'] = this.tabNumber;
    data['UserId'] = this.userId;
    data['UserTaskCategoryName'] = this.userTaskCategoryName;
    data['Id'] = this.id;
    data['UserTaskParentCategoryId'] = this.userTaskParentCategoryId;
    data['Remarks'] = this.remarks;
    return data;
  }
}
