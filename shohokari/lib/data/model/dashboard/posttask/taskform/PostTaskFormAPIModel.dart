import 'package:aitl/data/model/dashboard/posttask/taskform/UserTaskItemFormsModel.dart';

class PostTaskFormAPIModel {
  bool success;
  ResponseData responseData;

  PostTaskFormAPIModel({this.success, this.responseData});

  PostTaskFormAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<UserTaskItemFormsModel> userTaskItemForms;

  ResponseData({this.userTaskItemForms});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserTaskItemForms'] != null) {
      userTaskItemForms = [];
      json['UserTaskItemForms'].forEach((v) {
        userTaskItemForms.add(new UserTaskItemFormsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userTaskItemForms != null) {
      data['UserTaskItemForms'] =
          this.userTaskItemForms.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
