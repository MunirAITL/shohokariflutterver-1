class PaymentMethodsModel {
  String accountName;
  String accountNumber;
  String bankName;
  String cardNumber;
  String creationDate;
  int id;
  String paymentMethodType;
  int status;
  String updatedDate;
  int userId;

  PaymentMethodsModel({
    this.accountName,
    this.accountNumber,
    this.bankName,
    this.cardNumber,
    this.creationDate,
    this.id,
    this.paymentMethodType,
    this.status,
    this.updatedDate,
    this.userId,
  });

  PaymentMethodsModel.fromJson(Map<String, dynamic> json) {
    accountName = json['AccountName'] ?? '';
    accountNumber = json['AccountNumber'] ?? '';
    bankName = json['BankName'] ?? '';
    cardNumber = json['CardNumber'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    id = json['Id'] ?? 0;
    paymentMethodType = json['PaymentMethodType'] ?? '';
    status = json['Status'] ?? 0;
    updatedDate = json['UpdatedDate'] ?? '';
    userId = json['UserId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AccountName'] = this.accountName;
    data['AccountNumber'] = this.accountNumber;
    data['BankName'] = this.bankName;
    data['CardNumber'] = this.cardNumber;
    data['CreationDate'] = this.creationDate;
    data['Id'] = this.id;
    data['PaymentMethodType'] = this.paymentMethodType;
    data['Status'] = this.status;
    data['UpdatedDate'] = this.updatedDate;
    data['UserId'] = this.userId;
    return data;
  }
}
