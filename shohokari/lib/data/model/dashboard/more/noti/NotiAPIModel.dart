import 'NotiModel.dart';

class NotiAPIModel {
  bool success;
  ResponseData responseData;

  NotiAPIModel({this.success, this.responseData});

  NotiAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<NotiModel> notifications;
  ResponseData({this.notifications});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['Notifications'] != null) {
      notifications = [];
      json['Notifications'].forEach((v) {
        try {
          notifications.add(new NotiModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.notifications != null) {
      data['Notifications'] =
          this.notifications.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
