import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';

class TaskInfoSearchAPIModel {
  ErrorMessages errorMessages;
  ErrorMessages messages;
  bool success;
  ResponseData responseData;

  TaskInfoSearchAPIModel(
      {this.errorMessages, this.messages, this.success, this.responseData});

  TaskInfoSearchAPIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<TaskModel> locations;
  ResponseData({this.locations});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['Locations'] != null) {
      locations = [];
      json['Locations'].forEach((v) {
        locations.add(new TaskModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.locations != null) {
      data['Locations'] = this.locations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
