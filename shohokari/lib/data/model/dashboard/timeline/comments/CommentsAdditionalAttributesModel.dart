class CommentsAdditionalAttributesModel {
  ResponseData responseData;
  bool success;
  CommentsAdditionalAttributesModel({this.responseData, this.success});
  CommentsAdditionalAttributesModel.fromJson(Map<String, dynamic> json) {
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
    success = json['Success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    data['Success'] = this.success;
    return data;
  }
}

class ResponseData {
  List<Images> images;

  ResponseData({this.images});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['Images'] != null) {
      images = new List<Images>();
      json['Images'].forEach((v) {
        images.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.images != null) {
      data['Images'] = this.images.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Images {
  bool canComment;
  String dateCreatedLocal;
  String dateCreatedUtc;
  bool fullyLoaded;
  int likeStatus;
  int id;
  int mediaType;
  String mimeType;
  int nextMediaId;
  String thumbnailUrl;
  int totalComments;
  int totalLikes;
  String url;
  String attachmentType;

  Images(
      {this.canComment,
      this.dateCreatedLocal,
      this.dateCreatedUtc,
      this.fullyLoaded,
      this.likeStatus,
      this.id,
      this.mediaType,
      this.mimeType,
      this.nextMediaId,
      this.thumbnailUrl,
      this.totalComments,
      this.totalLikes,
      this.url,
      this.attachmentType});

  Images.fromJson(Map<String, dynamic> json) {
    canComment = json['CanComment'] ?? false;
    dateCreatedLocal = json['DateCreatedLocal'] ?? '';
    dateCreatedUtc = json['DateCreatedUtc'] ?? '';
    fullyLoaded = json['FullyLoaded'] ?? false;
    likeStatus = json['LikeStatus'] ?? 0;
    id = json['Id'] ?? 0;
    mediaType = json['MediaType'] ?? 0;
    mimeType = json['MimeType'] ?? '';
    nextMediaId = json['NextMediaId'] ?? 0;
    thumbnailUrl = json['ThumbnailUrl'] ?? '';
    totalComments = json['TotalComments'] ?? 0;
    totalLikes = json['TotalLikes'] ?? 0;
    url = json['Url'] ?? '';
    attachmentType = json['attachmentType'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CanComment'] = this.canComment;
    data['DateCreatedLocal'] = this.dateCreatedLocal;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['FullyLoaded'] = this.fullyLoaded;
    data['LikeStatus'] = this.likeStatus;
    data['Id'] = this.id;
    data['MediaType'] = this.mediaType;
    data['MimeType'] = this.mimeType;
    data['NextMediaId'] = this.nextMediaId;
    data['ThumbnailUrl'] = this.thumbnailUrl;
    data['TotalComments'] = this.totalComments;
    data['TotalLikes'] = this.totalLikes;
    data['Url'] = this.url;
    data['attachmentType'] = this.attachmentType;
    return data;
  }
}
