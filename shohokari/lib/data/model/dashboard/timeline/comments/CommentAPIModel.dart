import 'UserCommentPublicModelList.dart';

class CommentAPIModel {
  bool success;
  UserCommentPublicModelList comment;
  CommentAPIModel({this.success, this.comment});
  CommentAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    comment = json['Comment'] != null
        ? new UserCommentPublicModelList.fromJson(json['Comment'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.comment != null) {
      data['Comment'] = this.comment.toJson();
    }
    return data;
  }
}
