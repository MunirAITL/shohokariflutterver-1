class EN {
  get() {
    return {
      //  Keep these on top for enum popup menu
      'Edit': 'Edit',
      'Share task': 'Share task',
      'Copy': 'Copy',
      'Report': 'Report',
      'Cancel task': 'Cancel task',
      //  Tabbar
      'tab_new_task': 'Post a task',
      'tab_mytasks': 'My Tasks',
      'tab_findwork': 'Find Work',
      'tab_msg': 'Messages',
      'tab_more': 'More',
      //  Status code
      'status_receipt': 'Receipt',
      'status_payment': 'Payment',
      'status_post': 'POST',
      'status_purchase_order': 'PURCHASE ORDER',
      'status_active': 'ACTIVE',
      'status_assigned': 'ASSIGNED',
      'status_paid': 'PAID',
      'status_cancelled': 'CANCELLED',
      'status_deleted': 'DELETED',
      'status_alltask': 'ALL TASK',
      'status_pendingtask': 'PENDING TASK',
      'status_draft': 'DRAFT',
      'status_private_msg': 'PRIVATE MESSAGE',
      'status_req_payment': 'REQUEST PAYMENT',
      'status_rec_payment': 'RECEIVED PAYMENT',
      'status_funded': 'FUNDED',
      'status_cash': 'CASH',
      'status_funded_req_payment': 'FUNDED REQUEST PAYMENT',
      'status_release_payment': 'FUNDED RELEASE PAYMENT',
      'status_verified': 'VERIFIED',
      'status_unverfied': 'UNVERIFIED',
      //  Welcome Page
      'loading': 'Loading...',
      'lng': 'Eng',
      'introduction_screen_catch_phrase_title': 'Welcome to the Shohokari app',
      'introduction_screen_catch_phrase_text':
          'By using the Shohokari app you can get many different types of work done.',
      'start': 'Let\'s Start',
      //  Login Page
      'join_shohokari_title': 'Join Shohokari',
      'sign_up_create_account': 'Create an account',
      'log_in': 'Log In',
      'login': 'Login',
      'introduction_screen_login_button': 'Log in',
      'email_or_phone_number': 'Email or Phone Number',
      'password': 'Password',
      'sign_up_login_button_forgot_password': 'Forgot password ?',
      'sign_up_login_separator_or': 'or',
      'or': 'Or',
      'sign_up_login_main_button_label_mobile_login': 'Log in with Mobile',
      'fb_login': 'Log in with Facebook',
      'com_facebook_loginview_log_in_button_continue': 'Continue with Facebook',
      'com_google_loginview_log_in_button_continue': 'Continue with Google',
      'missing_email': 'Missing email',
      'dialog_password_reset_title': 'Password reset',
      'forgot_pwd_txt1': 'Send an email to ',
      'forgot_pwd_txt2': ' with instruction to reset your password?',
      'ok': 'Ok',
      'cancel': 'Cancel',
      'missing_ep': 'Missing email/phone',
      'pwd_wrong': 'Password should be greater by 4 characters',
      'dob_invalid': 'Invalid date of birth',
      'mobile_invalid': 'Invalid mobile number',
      'email_not_exists': 'Email address does not exists',
      'lname_invalid': 'Invalid last name',
      'fname_invalid': 'Invalid first name',
      'fullname_invalid': 'Invalid full name',
      'create_profile_first_name_hint': 'First name',
      'create_profile_last_name_hint': 'Last name',
      'email_txt': 'Email',
      'mobile_number_label': 'Mobile number',
      'email_invalid': 'Invalid email address',
      'tc_reg1': 'By signing up, I agree to ',
      'tc_reg2': ' Terms & Conditions',
      'tc': 'Terms & Conditions',
      'tc_sms1':
          'By clicking on \'Log in with Mobile\' you confirm that you accept the ',
      'login_page_title':
          'Please enter your mobile number to start using the app',
      'please_give_your_mobile_number':
          'You can log in by entering your mobile number',
      'add_mobile_number_mobile_number_hint': 'Enter your mobile number',
      'next_btn': 'Next',
      'otp_3_sent': 'Enter the code that was sent to',
      'sms_verification_resendcode_title': 'I didn\'t get a code',
      'loading': 'Loading...',
      'create_profile': 'Create profile',
      'confirm_your_offer_add_date_of_birth': 'Provide a date of birth',
      'create_profile_dob_info':
          'To sign up, you must be 18 or older. Other people won\'t see your birthday.',
      'post_task_schedule_select_due_date': 'Select a date',
      'create_profile_your_location_label': 'Enter your area',
      'pick_addr': 'Please pick your address',
      'on_boarding_poster_four_continue_button_label': 'Get started',
      'search': 'Search',
      'create_profile_gender': 'Gender',
      'create_profile_gender_type_male': 'Male',
      'create_profile_gender_type_female': 'Female',
      //  Post Task
      'tutorial_text_task_post':
          'Select the type of job you need done from below',
      'navigation_bar_post_task_label': 'Post a Task',
      'task_post_i_am_worker': 'I want to work',
      'task_post_i_need_employee': 'I want to hire',
      //  Task list...
      'suggestions_cleaning_pest_control_label': 'Cleaning',
      'suggestion_example_cleaning_pest_control':
          'E.g. Clean my 2 bedroom apartment',
      'suggestions_anything_else_label': 'Anything',
      'suggestion_example_anything_else':
          'E.g. Suggest a name for my new company etc.',
      'browse_tasks_chip_categories_all_categories': 'All categories',
      'building_contruction': 'Building and Construction',
      'building_contruction_txt':
          'E.g. Need an extra hand with laying my patio',
      'housemaids': 'Housemaids',
      'housemaids_txt': 'E.g. Take care of my children Monday-Friday daytime',
      'repairs': 'Repairs',
      'repairs_txt': 'E.g. Can someone fix my washing machine or tv!',
      'suggestions_part_time_job_label': 'Part Time Job',
      'suggestion_example_part_time_job': 'E.g. Need few sales representatives',
      'delivery_removals': 'Delivery and Removals',
      'delivery_removals_txt':
          'E.g. Moving home and I need someone to take my heavy furniture for me',
      'suggestions_computer_it_label': 'Computer/IT',
      'suggestions_computer_it_label_txt':
          'E.g. Need to fix my computer/ Need do write an article',
      'suggestions_cleaning_pest_control_label_1': 'Pest Control',
      'suggestions_cleaning_pest_control_label_1_txt':
          'E.g. Fruitfly infestation- SOS!',
      'suggestions_shifting_moving_label': 'Shifting/Moving',
      'suggestion_example_shifting_moving':
          'E.g. Need a truck for shifting etc.',
      'hair_beauty': 'Hair and Beauty',
      'hair_beauty_txt': 'E.g. Need my nails done ASAP!',
      'tutor_trainer': 'Tutor & Trainer',
      'tutor_trainer_txt': 'E.g. Need a tutor for teaching English',
      'suggestions_marketing_label': 'Marketing',
      'suggestions_marketing_label_txt':
          'E.g. Need someone to propose a plan for marketing campaign',
      'suggestions_tailor_label': 'Tailor',
      'suggestion_example_tailor':
          'E.g. Need tailor for making a few shirts etc.',
      'suggestions_driver_car_rental_label': 'Hire Driver',
      'suggestion_example_car_rental_it': 'E.g. Need a Sedan for 6 hours etc.',
      'task_category_car_label': 'Rent Car',
      'task_category_truck_medium_label': 'Rent Truck',
      'security_gaurd': 'Security Gaurd',
      'suggestion_example_security_gaurd':
          'E.g. Need secuirty guard for office etc.',
      'suggestions_admin_label': 'Admin',
      'suggestion_example_admin': 'E.g. Need an admin assistant',
      'suggestions_shopping_control_label': 'Shopping',
      'suggestions_shopping_control_label_txt':
          'E.g. I need someone to do some shopping for me every Monday',
      'events_entertainment': 'Events and Entertainment',
      'events_entertainment_txt': 'E.g. Can someone DJ for my wedding?',
      'suggestions_laundry_label': 'Laundry',
      'suggestion_example_laundry_service': 'E.g. Need laundry service etc.',
      'hire_goods': 'Hire Goods',
      'hire_goods_txt': 'E.g. Need to hire a marquee for a garden party!',
      'other': 'Other',
      'other_txt': 'E.g. Suggest a name for my new company etc.',
      //  Post Task 1
      'post_task_screen_title_new': 'Post Task',
      'delete_task_confirm': 'Are you sure, you want to delete this task?',
      'post_task_segment_title_details': 'DETAILS',
      'post_task_segment_title_date': 'DUE DATE',
      'post_task_segment_title_price': 'BUDGET',
      'continue': 'Continue',
      'task_headline': 'Task Headline',
      'contact_fragment_description_hint': 'Description',
      'online_job': 'Will the job be based online?',
      'map_action_bar_title': 'Task location',
      'yes': 'Yes',
      'no': 'No',
      'post_task_details_error_title':
          'Please enter a valid title(at least 10 characters)',
      'post_task_details_error_description':
          'Please enter a valid description (at least 25 characters)',
      //  Post Task 2
      'post_task_schedule_due_date_when_do_you_label':
          'When do you need it done?',
      'propose_new_time_due_date_label': 'Due date',
      //  Post Task 3
      'post_task_price_invalid_price_text': 'Task allowed estimated price is ',
      'post_task_price_rate_label': 'Price per hour',
      'post_task_price_error_hours': 'Please enter valid hours',
      'post_task_price_state_label_total': 'Total',
      'post_task_price_state_label_hourly_rate': 'Hourly rate',
      'post_task_price_price_label':
          'What is your estimated budget?\n(Your budget must be between ',
      'post_task_price_hours_label': 'Hours',
      'taskers_need': 'How many taskers do you need?',
      'post_task_price_estimated_budget_label': 'Estimated budget:',
      'post_task_price_per_tasker_label': 'Per shohokari',
      'offer_accepted_confirmation_title': 'Offer accepted',
      'offer_accept': 'Offer accept',
      'accept_payment_methods_accept_button': 'Accept & add funds',
      'done': 'Done',
      'submit': 'Submit',
      'offer_successful_title': 'Congratulations!',
      //  Post Task 4
      'post_task_finished_title_message': 'Congratulations!',
      'post_task_finished_your_task_was_posted_text':
          'Your task has been posted. You\'ll be notified as soon as you receive an offer.',
      'task_attachments': 'Task attachments',
      'attachments': 'Attachments',
      'chooseopt': 'Choose an option',
      'view': 'View',
      'delete': 'Delete',
      'files_added': 'files added',
      'max_file_limit': 'Maximum file upload limit is',
      'files': 'files',
      'add_upto5_files': 'Add up to 5 files',
      'confirmation': 'Confirmation',
      'confirm_del_task_attachment':
          'Are you sure, you want to remove this attachment?',
      'resolution_post_msg':
          'Thank you for submitting the support ticket.\n\nOur support member will contact you as soon as possible.',
      //  Cam
      'choose_cam': 'Choose image source',
      'gallery': 'Gallery',
      'camera': 'Camera',
      'from_gallery': 'From Gallery',
      'from_cam': 'From Camera',
      'skip': 'Skip',
      //  My Tasks
      'navigation_bar_my_tasks_label': 'My Tasks',
      'search_by_title': 'Search by title',
      'all': 'All',
      'posted': 'Posted',
      'draft': 'Draft',
      'assigned': 'Assigned',
      'offered': 'Offered',
      'completed': 'Completed',
      'mytasks_nf':
          'Looks like you haven\'t posted a task or made any offers just yet. How about doing that now?',
      //  Find works
      'browse_tasks_screen_title': 'Find Work',
      'tutorial_text_browse_task':
          'To earn money select suitable job from below',
      'findworks_nf':
          'We couldn\'t find any tasks that match - may be try a different filter or search word?',
      'browse_tasks_empty_state_change_filters': 'Change filters',
      //  Filters
      'browse_tasks_chip_task_type_all': 'In person & remotely',
      'browse_tasks_chip_task_location': '500km, dhaka DHAKA',
      'browse_tasks_chip_task_price_any': 'Any price',
      'browse_tasks_chip_task_state_open': 'Open tasks',
      'browse_tasks_filter_screen_title': 'Filters',
      'apply': 'Apply',
      'browse_tasks_filter_task_type_task_with_location': 'In person',
      'browse_tasks_filter_task_type_online': 'Remotely',
      'browse_tasks_filter_task_type': 'To be done',
      'browse_tasks_map_location_label': 'Location',
      'distance': 'Distance',
      'price': 'Price',
      'browse_tasks_filter_task_status_description': 'Available tasks only',
      'browse_tasks_filter_task_status_sub_description':
          'Hide tasks that are already assigned',
      'on': 'On',
      'off': 'Off',
      //  Private Messages
      'private_conversation_screen_title': 'Private messages',
      'message_shohokari_support_team_contact':
          'Contact with Shohokari Support Team',
      'msg_nf':
          'You haven\'t got any messages yet - assign a task or get assigned to chat privately!',
      'refresh': 'Refresh',
      //  More
      'more': 'More',

      'dashboard': 'Dashboard',
      'profile': 'Profile',
      'payment_history': 'Payment History',
      'payment_methods': 'Payment Methods',
      'reviews': 'Reviews',
      'notifications': 'Notifications',
      'task_alerts_setting': 'Task Alerts Setting',
      'settings': 'Settings',
      'help': 'Help',
      'logout': 'Logout',
      'log_out': 'Log out',
      'confirm_logout': 'Are you sure you want to log out?',
      //  settings
      'edit_profile_screen_title': 'Edit account',
      'notifications_settings_screen_title': 'Notification settings',
      //  dashboard
      'as_tasker': 'As a Tasker',
      'as_poster': 'As a Poster',
      'dashboard_column_bid_on_label': 'Bid On',
      'dashboard_column_open_for_offers_label': 'Open for offers',
      'dashboard_tab_overdue': 'Overdue',
      'dashboard_tab_awaiting_payments': 'Awaiting Payments',
      'dialog_completion_rate_message_runner':
          'Completion rate is the percentage of tasks assigned to the Shohokari which were successfully completed',
      'dialog_completion_rate_message_sender':
          'Completion rate is the percentage of tasks assigned by the poster which were successfully completed',
      'noti_nf':
          'This is where we\'ll let you know about tasks, comments and other stuff. How about posting a task or finding one to do through browse tasks?',
      //  noti
      'commented_on': ' commented on ',
      'completion_rate': 'Completion Rate',
      'completion_rate_none': 'No Completion Rate',
      'task_posted_matching_skills':
          'A new task has been posted matching your skills: ',
      'list_item_comment_popup_menu_report_label': 'Report',
      'task_details_reviews_someone_left_a_review_for_someone':
          ' left a review for ',
      // noti settings
      'notifications_settings_test_header': 'Is it working?',
      'notifications_settings_test_send_button': 'Test it',
      'notifications_settings_test_description':
          'Make sure you\'re actually getting those all important push notifications.',
      'notifications_settings_section':
          'Your notifications can be updated at any time via the options below',
      //  noti settings array
      'notifications_settings_label_transactional': 'Transactional',
      'notifications_settings_label_task_updates': 'Task updates',
      'notifications_settings_label_task_reminders': 'Task reminders',
      'alerts': 'alerts',
      'notifications_settings_label_task_recommendations':
          'Task recommendations',
      'notifications_settings_label_helpful_information': 'Helpful information',
      'notifications_settings_label_updates_newsletters':
          'Updates & newsletters',
      'notifications_settings_transactional_details':
          'You will always receive important notifications about any payments, cancellations and your account.',
      'notifications_settings_task_updates_details':
          'Receive updates on any new comments, private messages, offers and reviews.',
      'notifications_settings_task_reminders_details':
          'Friendly reminders if you\'ve forgotten to accept an offer, release a payment or leave a review.',
      'notifications_settings_details_Shohokari_alerts':
          'Once you\'ve set up your Shohokari Alerts, you\'ll be instantly notified when a task is posted that matches your requirements.',
      'notifications_settings_details_task_recommendations':
          'Receive recommendations and be inspired by tasks close to you.',
      'notifications_settings_updates_newsletters_details':
          'Be the first to hear about new features and exciting updates on Shohokari.',
      'email_noti': 'EMAIL',
      'sms_noti': 'SMS',
      'push_noti': 'PUSH',
      '': '',
      '': '',
      //  noti alert types
      'push_notification_task_alert':
          '[Task Alert] #name# has posted this task:',
      'push_notification_make_offer':
          '[Make Offer] #name# has made an offer for your task',
      'push_notification_task_comment':
          '[Task Comments] #name# has commented on your task',
      'push_notification_private_message':
          '[Private Message] #name# has sent a message on your task',
      'push_notification_offer_message':
          '[Offer Message] #name# has sent a message on your offer',
      'push_notification_accept_offer':
          '[Accept Offer] #name# has accepted your offer for task',
      'push_notification_payment_confirmation':
          '[Payment Confirmation] #name# has received payment for task',
      'push_notification_user_review':
          '[User Review] #name# has given rating and review for task',
      //  profile
      'myprofile': 'My Profile',
      'task_alerts_edit_online': 'Online',
      'profile_last_online_format': 'Last online ',
      'member_since': 'Member since ',
      'profile_button_request_a_quote': 'Request a quote',
      'profile_report_user_button_label': 'Report this member',
      'profile_no_reviews_runner':
          'Looks like you haven\'t received any reviews just yet.',
      'profile_user_no_reviews_runner':
          ' hasn\'t received any reviews just yet.',
      'profile_no_reviews_created':
          'Looks like you haven\'t created any reviews just yet',
      'profile_user_no_reviews_created':
          ' hasn\'t created any reviews just yet',
      'view_badges_badges_label': 'BADGES',
      'profile_screen_portfolio_section_label': 'PORTFOLIO',
      'portfolio': 'Portfolio',
      'full_screen_image_screen_title': 'Attachment',
      'view_user_no_badges_yet':
          'Where are the ID Badges? None have been applied yet.',
      'task_details_missing_badge_learn_more': 'Learn More',
      'skills_label': 'SKILLS',
      'transportation_label': 'Transportation',
      'languages_label': 'Languages',
      'education_label': 'Education',
      'work_label': 'Work',
      'specialities_label': 'Specialities',
      //  edit profile
      'profile_updated_alert': 'Profile updated successfully.',
      'pick_loc': 'Please pick your location',
      'dialog_discard_changes_title': 'Unsaved changes',
      'dialog_discard_changes_message': 'Do you want to discard the changes?',
      'edit_profile': 'Edit profile',
      'save': 'Save',
      'deactive_acc': 'Deactivate My Account',
      'general_information_label': 'General information',
      'private_information_label': 'Private information',
      'additional_information_label': 'Additional information',
      'reason': 'Reason',
      'deactivate': 'Deactivate',
      'headline': 'Headline',
      'edit_profile_about_me_label': 'Brief bio',
      'email_not_verified': 'Email not verified.\nPlease verify your Email.',
      'dob': 'Date of birth',
      'current_mobile_number_label': 'Current mobile number? *',
      'portfolio_item_delete': 'Portfolio Item Delete',
      'portfolio_del_confirm':
          'Are you sure, you want to delete this portfolio?',
      'means_transport': 'Select your means of transport?',
      'languages_versed': 'What languages you\'re versed in?',
      'education_input_hint': 'Where have you studied?',
      'work_input_hint': 'Where have you previously worked?',
      'specialities_input_hint': 'What tasks are you great at?',
      'enter_skills': 'Please enter some skill',
      'add': 'Add',
      //  Task details
      'task_details': 'Task details',
      'active': 'Active',
      'paid': 'Paid',
      'cancelled': 'Cancelled',
      'posted_by': 'POSTED BY',
      'task_details_view_map': 'View map',
      'task_loc': 'Task location',
      'map_get_directions': 'Get directions',
      'reschedule': 'Reschedule',
      'approx': 'Approx',
      'hrs': 'hrs',
      'per_shohokari': 'per Shohokari',
      'task_price': 'TASK PRICE',
      'task_price2': 'Task price',
      'make_offer': 'Make an offer',
      'review_offer': 'Review Offers',
      'task_details_prompt_info':
          'You will be prompted to confirm the release of payment',
      'release_payment': 'Release payment',
      'receive_payment': 'Receive payment',
      'request_payment': 'Request payment',
      'request': 'Request',
      'funded': 'Funded',
      'request_release_payment': 'Request release payment',
      'update_offer': 'Update offer',
      'increase_price': 'Increase price',
      'task_details_open_private_messages_button': 'Open private messages',
      'task_summary': 'TASK SUMMARY',
      'spots_left': 'Spots Left',
      'offers': 'Offers',
      'view_all_offers_button': 'View all offers',
      'task_details_awaiting_offers_label': 'Waiting for offers',
      'reply': 'Reply',
      'withdraw_offer': 'Withdraw',
      'are_you_sure_you_want_to_withdraw_your_offer':
          'Are you sure you want to withdraw your offer?',
      'review': 'Review',
      'accept': 'Accept',
      'release': 'Release',
      'comments': 'Comments',
      'askq': 'Ask #name# a question',
      'donot_share_personal_info_msg':
          'Anyone using Shohokari can see these comments. For your own safety, please do not share any personal information here.',
      'dialog_cancel_posted_task_message':
          'Are you sure you want to cancel your task?',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      //  task alert keyword
      'kw_long': 'keyword needs to be at least 3 letters long',
      //  payment
      'payment_medthod_card': 'Bank/Card',
      'payment_medthod_cash': 'Cash',
      //  payment history
      'more_options_payment_history_label': 'Payment history',
      'payment_history_tab_title_earned': ' Earned ',
      'payment_history_tab_title_outgoing': ' Outgoing ',
      'payment_history_showing_label': 'Showing:',
      'task_receipt_net_earning_label': 'Net earnings',
      'task_receipt_net_paid_label': 'Net paid',
      'file': 'file',
      'file_downloaded': 'File download successfully.',
      'payment_history_nf': 'Payment history not found',
      'payment_history_download_button': 'Download',
      'payment_history_no_transactions_filtered':
          'You don\'t have any earned to show here. Change your filter settings above.',
      'payment_history_no_transactions_outgoing':
          'You haven\'t paid for any tasks yet.',
      'task_receipt_funds_released_on_label': 'Funds released on:',
      'task_receipt_funds_secured_on_label': 'Funds secured on:',
      'task_receipt_payment_method_label': 'Payment method:',
      'All time': 'All time',
      'Last financial year': 'Last financial year',
      'Current financial year': 'Current financial year',
      'Last quarter': 'Last quarter',
      'Current quarter': 'Current quarter',
      //  Payment settings
      'payment_settings': 'Payment settings',
      'payment_settings_tab_title_make_payments': 'Make payments',
      'payment_settings_tab_title_receive_payments': 'Receive payments',
      'payment_setting_add_promotion_code': 'Add promotion Code',
      'add_billing_address': 'Add billing address',
      'add_bank_account_screen_title_add': 'Add bank account',
      'mobile_payment_screen_title_bikash': 'Add bKash Account',
      'mobile_payment_screen_title_rocket': 'Add Rocket Account',
      'add_Shohokari_card': 'Add promo card',
      'add_shohokari_card_code_hint': 'Enter a promo/gift code',
      'code': 'Code',
      'empty_promotion_code_error_message': 'Promotion Code cannot be blank',
      'promotion_code_added_message': 'Promotion Code is successfully added',
      'payment_setting_invalid_promotion_code': 'Invalid Promotion Code',
      'payment_setting_invalid_promotion_code_messgae':
          'Please enter a valid Promotion Code',
      'billing_address_button_change': 'Change billing address',
      'billing_address_au_address_line_1_hint': 'Address line 1',
      'billing_address_au_address_line_2_hint': 'Address line 2 (optional)',
      'area': 'Area',
      'city': 'City',
      'address': 'Address',
      'postcode': 'Post Code',
      'country': 'Country',
      'billing_addr_added': 'Billing Address added successfully.',
      'billing_addr_updated': 'Billing Address updated successfully.',
      'add_bank_account': 'Add bank account',
      'account_name': 'Account name',
      'bank_name': 'Bank name',
      'account_number': 'Account number',
      'add_bank_account_active_checkbox_label': 'Set as default payment method',
      'add_bank_account_error_account_name':
          'Please enter a valid account name',
      'add_bank_account_error_bank_name': 'Please enter a valid bank name',
      'add_bank_account_error_account_length_gb':
          'Please enter a valid account number',
      'bank_acc_updated': 'Bank Account updated successfully.',
      'bank_acc_added': 'Bank Account added successfully.',
      'mobile_payment_error_number': 'Please enter a valid number',
      'bikash_added_msg': 'Bikash Account added successfully.',
      'bikash_updated_msg': 'Bikash Account updated successfully.',
      'rocket_added_msg': 'Rocket Account added successfully.',
      'rocket_updated_msg': 'Rocket Account updated successfully.',
      //  Reviws
      'reviews_screen_empty_state': 'There are no reviews to show!',
      //  Task Alerts
      'task_alerts': 'Task alerts',
      'list_item_task_remote_label': 'Remote',
      'within': 'Within ',
      'alerts_kw_nf_title':
          'If you add Keyword from below to the type of job you want to be notified, then you will receive notification immediately posting such type of work.',
      'alerts_kw_nf':
          'a task that you\'re interested in. Add keywords such as moving or clean.',
      'add_task_alert': 'Add task alert',
      'edit_task_alert': 'Edit task alert',
      'dialog_task_alert_delete_title': 'Delete alert',
      'dialog_task_alert_delete_message':
          'Are you sure you want to delete this alert?',
      'add_alert': 'Add alert',
      'update_alert': 'Update alert',
      'kw_phrase': 'Keyword or phrase (Driver, Clean, Part Time, Web etc)',
      'enter_kw_phrase': 'Enter keyword or phrase',
      //  Help
      'support_centre': 'Support centre',
      'terms_and_conditions': 'Terms & Conditions',
      'privacy_screen_title': 'Privacy',
      'guide_around_shohokari': 'Guide around Shohokari',
      'guide_become_tasker': 'Guide to becoming a tasker',
      'tasker_tutorial': 'Shohokari/Tasker Tutorial (Video)',
      'poster_tutorial': 'Task Poster Tutorial (Video)',
      'support_activity_title': 'Support',
      'call': 'Call',
      'send_message': 'Send Message',
      'contact_us': 'Contact us',
      'how_help_you': 'How can we help you?',
      'choose_ticket_type': 'Please choose ticket type from the list',
      'pls_enter_desc': 'Please enter description',
      'contact_from_app': 'Contact from App',
      //  Contact us
      'select_sup_ticket_type': 'Select Support Ticket Type',
      //  Tutorial 1
      'tutorial_text_1':
          'To find assistant for necessary work tap on the Post A Task Icon',
      'tutorial_text_2':
          'To see all your work together tap on the My tasks icon',
      'tutorial_text_3': 'To work as a Shohokari tap on the Browse tasks icon',
      'tutorial_text_4': 'To view your messages tap on the Messages icon',
      'tutorial_text_5':
          'Tap on More icon for your profile, payment notifications, and other settings',
      'tutorial_text_6':
          'Tap the Task alerts icon to get your favorite work alert as a Shohokari',
      'finish': 'Finish',
      'continue': 'Continue',
      //  Tutorial 2
      'hi': 'Hi',
      'lets_get_earning_title': 'let\'s get you earning!',
      'on_boarding_runner_one_message':
          'Set up your profile with a photo & description so Job Posters know who you are. Take the time to fill it so they choose you!',
      'on_boarding_tasker_two_title': 'Find a task',
      'on_boarding_tasker_two_message':
          'Browse tasks to find work you can complete. Check what needs to be done, where and when and make an offer.',
      'on_boarding_tasker_three_title': 'Make an offer',
      'on_boarding_tasker_three_message':
          'When making an offer, say why you\'re great for the task, suggest a fair price and answer any questions.',
      'on_boarding_tasker_four_title': 'Do the task well, get paid!',
      'on_boarding_tasker_four_message':
          'Now your offer\'s accepted and payment is secured with Shohokari Pay, get it done then ask for payment release!',
      'back': 'Back',
      'previous': 'Previous',
      //  Chat
      'questions': 'Questions',
      'private_messages': 'Private messages',
      'poster': 'POSTER',
      'private_conversation_messages_closed_message':
          'Task completed. Private messages closed',
      'private_conversation_messages_cancel_message':
          'Task cancelled. Private messages closed',
      'hint_enter_a_message': 'Type a message…',
      'offer_chat': 'Offer chat',
      //  Offer Comments
      'task_paid_offer_closed': 'Task PAID. Offer messages closed',
      'task_cancel_offer_closed': 'Task cancelled. Offer messages closed',
      'reply_to': 'Reply to #name#',
      'send': 'Send',
      'chat': 'Chat',
      //  Receive Payment
      'review_additional_funds_payment_methods': 'Payment methods',
      //  Resolution Page
      'flag_screen_title_task': 'Report Task',
      'flag_source_comment': 'Report Comment',
      'flag_screen_title_user': 'Report User',
      'flag_screen_send_report_button_label': 'Send report',
      'flag_screen_please_enter_a_comment_message': 'Please enter a comment',
      'select_report_type': 'Select Report Type',
      'submitted_successfully': 'Submitted successfully',
      'empty_description_error_message': 'Description cannot be blank',
      'offer': 'Offer',
      'cash_payment': 'Cash payment',
      'cash_payment_message': 'Please directly pay cash to #name#',
      'choose_payment_opt': 'Please choose payment option from the list',
      'release_payment_payment_secured': 'PAYMENT SECURED',
      'offer_accepted_confirmation_funds_secured_text':
          'The funds for the task have now been secured. You\'re in control to release the funds once the task is completed. No further payment for the task is required.',
      'offer_accepted_cash_confirmation_funds_secured_text':
          'Your payment will be paid cash directly to #name#, who will confirm the received payment',
      'release_payment_terms':
          'Please note that by releasing payment you\'re agreeing to our payment terms and conditions.',
      'funded_payment_methods': 'Select Payment Type',
      'received': 'Received',
      'message_for_payment_request__title_label':
          'Please make sure you have completed your task properly before requesting payment. Otherwise it may bother your client.',
      'message_for_payment_request_label': 'Message for Payment',
      'requested_release_payment_message':
          ' You are requested to Release Payment for the task since I have completed your task : ',
      'requested_payment_message':
          ' You are requested to Make Payment for the task since I have completed your task : ',
      //  Review Page
      'leave_review': 'Leave review',
      'leave_review_button_label': 'Submit review',
      'leave_review_leave_review_for': 'Leave a review for ',
      'leave_review_rating_bar_label':
          'Tap the number of stars for this review',
      'leave_review_review_hint': 'Please leave a review here',
      //  Accept Offer
      'accept_offer': 'Accept offer',
      'by_accepting_offer_text':
          'By accepting this offer, you agree to pay the Tasker for the agreed job. You may be charged a fee if you discontinue the Task after accepting it.',
      'no_offers_yet_message':
          'You have not yet received any Offer for this Task',
      'update_make_offer': 'Update an offer',
      'make_offer_your_offer_label': 'Your offer:',
      'make_offer_why_you_the_best_label':
          'Why are you the best person for this task?',
      'make_offer_you_will_receive': 'You\'ll receive: #amt#',
      'make_offer_service_fee': '(Service fee: #srv_fee#)',
      'offers_nf': 'You have not yet received any Offers for this Task',
      'others': 'Others',
      'confirm_your_offer': 'Confirm your offer',
      'pending_verification': 'Pending Verification!',
      'no_yet_verified_user_message_message':
          'Your information is pending verification. Do you want to contact us?',
      //  User Role
      'user_role': 'User Role',
      'user_role_header': 'What is your role?',
      'employer': 'Employer',
      'user_role_employer_label': 'I want to give work',
      'worker': 'Worker',
      'user_role_worker_label': 'I want to work and earn money',
      //  Case Alert
      'case_alert': 'Case alert',
      'close': 'Close',
      'date': 'date',
//  Confirm Offer Opt via Banner
      'confirm_your_offer_add_profile_picture': 'Upload a profile picture',
      'confirm_your_offer_add_bank_account':
          'Please provide your payment information',
      'confirm_your_offer_add_nid_number':
          'Please upload your National ID Card image',
      'success': 'Success',
      'error': 'Error',
      '': '',
      '': '',
      '': '',
      '': '',
    };
  }
}
