import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/noti/NotificationData.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';

class NotiHelper with Mixin {
  Map<String, dynamic> getNotiMap({NotiModel model}) {
    Map<String, dynamic> notiMap = {};
    try {
      // print("Get Noti Map = "+model.toMap().toString());
      // print(NotiCfg.EVENT_LIST.toString());
      /*for (var map in NotiCfg.EVENT_LIST) {
        final int listCommunityId = map["communityId"];
        int notificationEventId = map['notificationEventId'];
        if (listCommunityId == int.parse(userData.userModel.communityID)) {
          if (model.entityName == map["entityName"] &&
              (model.notificationEventId == notificationEventId ||
                  notificationEventId == 0)) {
            String txt = map["txt"];
            txt = txt.replaceAll(
                "#InitiatorDisplayName#", model.initiatorDisplayName);
            txt = txt.replaceAll("#CaseNo#", model.entityId.toString());
            txt = txt.replaceAll("#EventName#", model.eventName);
            txt = txt.replaceAll('[] ', '');
// print("case No 1 =  "+txt);
            //  text processing
            notiMap['txt'] = txt ?? '';
            // print("case No 2 =  "+notiMap['txt']);

            notiMap['publishDateTime'] = DateFun.getTimeAgoTxt(model.publishDateTime);

            //  event processing
            //notiMap['communityId'] = map['communityId'];
            //notiMap['desc'] = map['desc'];
            //notiMap['notificationEventId'] = map['notificationEventId'];
            notiMap['route'] =
                (map['route2'] == null) ? map['route'] : map['route2'];

            break;
          }
        }
      }*/
    } catch (e) {
      print(e.toString());
    }
    return notiMap;
  }

  Map<String, dynamic> getNotiMapLocalNotification({NotificationData model}) {
    Map<String, dynamic> notiMap = {};
    try {
      /*for (var map in NotiCfg.EVENT_LIST) {
        int notificationEventId = map['notificationEventId'];

        if (model.entityName == map["entityName"] &&
            (model.notificationEventId.toString() ==
                    notificationEventId.toString() ||
                notificationEventId == 0)) {
          String txt = map["txt"];
          txt = txt.replaceAll("#InitiatorDisplayName#", model.initiatorName);
          txt = txt.replaceAll("#CaseNo#", model.entityId.toString());
          txt = txt.replaceAll("#EventName#", model.message);
          txt = txt.replaceAll('[] ', '');
          notiMap['txt'] = txt ?? '';

          break;
        }
      }*/
    } catch (e) {
      print(e.toString());
    }
    return notiMap;
  }
}

/*extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}*/
