import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTask3Controller extends GetxController {
  var totalAmounts = 0.0.obs;
  calculation({
    bool isSwitchFixedPrice,
    TextEditingController estBudget,
    TextEditingController priceHr,
    TextEditingController totalHr,
  }) {
    totalAmounts = 0.0.obs;
    if (isSwitchFixedPrice) {
      if (estBudget.text.isNotEmpty) {
        try {
          totalAmounts = double.parse(estBudget.text).obs;
        } catch (e) {
          totalAmounts = 0.0.obs;
        }
      }
    } else {
      try {
        if (priceHr.text.isNotEmpty && totalHr.text.isNotEmpty) {
          totalAmounts =
              (double.parse(priceHr.text) * double.parse(totalHr.text)).obs;
        }
      } catch (e) {
        totalAmounts = 0.0.obs;
      }
    }
  }
}
