import 'package:aitl/data/model/misc/RBGroupModel.dart';
import 'package:get/get.dart';

class OptSelController extends GetxController {
  var listCB = Map<int, List<Map<int, bool>>>().obs; //  generic checkbox
  var listRB = Map<int, int>().obs; //  generic radiobtn
}
