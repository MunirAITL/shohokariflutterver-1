import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/widgets/btn/MMArrowBtn.dart';
import 'package:aitl/view/widgets/images/ImgFade.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class WelcomePage extends StatefulWidget {
  @override
  State createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> with Mixin, StateListener {
  final audioController = Get.put(AudioController());

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("welcome_shohokari");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      audioController.dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: MyTheme.bgColor,
            elevation: 0,
            actions: drawSoundLngBox(this.runtimeType, audioController, []),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    drawHeaderImage(),
                    SizedBox(height: 10),
                    drawCenterView(),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 10, right: 10, top: 20),
                      child: MMArrowBtn(
                          txt: "start".tr,
                          bgColor: Colors.white,
                          txtColor: Colors.black,
                          height: getHP(context, MyTheme.btnHpa),
                          width: getW(context),
                          radius: 10,
                          icon: Icons.arrow_right,
                          callback: () {
                            Get.to(() => AuthScreen());
                          }),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  drawHeaderImage() {
    return Container(
      //width: getWP(context, 70),
      //height: getHP(context, 30),
      child: SvgPicture.asset('assets/images/welcome/shohokari_logo.svg'
          //fit: BoxFit.fill,
          ),
    );
  }

  drawCenterView() {
    return Container(
        child: Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Txt(
            txt: "introduction_screen_catch_phrase_title".tr,
            txtColor: Colors.white,
            txtSize: MyTheme.txtSize + .2,
            txtAlign: TextAlign.center,
            isBold: false,
            //txtLineSpace: 1.5,
          ),
        ),
        Container(
          width: getW(context),
          height: getHP(context, 30),
          child: ImgFade(url: "assets/images/welcome/welcome_banner.png"),
        ),
        Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: Txt(
                txt: "introduction_screen_catch_phrase_text".tr,
                txtColor: MyTheme.airBlueColor,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false
                //txtLineSpace: 1.5,
                ),
          ),
        ),
      ],
    ));
  }
}
