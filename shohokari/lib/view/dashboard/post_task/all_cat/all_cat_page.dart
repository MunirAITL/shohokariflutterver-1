import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildUserTaskCategorysModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentUserTaskCategorysModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add_form/addtaskform_page.dart';
import 'package:aitl/view/widgets/tile/ExpansionTileCard.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'all_cat_base.dart';

class AllCatPage extends StatefulWidget {
  const AllCatPage({Key key}) : super(key: key);
  @override
  State createState() => _AllCatState();
}

class _AllCatState extends BaseAllCatStatefull<AllCatPage>
    with APIStateListener {
  List<ParentUserTaskCategorysModel> listParentUserTaskCategorysModel;
  List<ChildUserTaskCategorysModel> listChildUserTaskCategorysModel;
  APIStateProvider _apiStateProvider;

  int parentId = 1;
  int indeXCardKey = 0;
  String appbarTitle = "browse_tasks_chip_categories_all_categories".tr;

  List<GlobalKey<ExpansionTileCardState>> listXCardKey = [];
  final ScrollController _scrollController = new ScrollController();

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.parent_all_cat &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              listParentUserTaskCategorysModel = (model as ParentAllCatAPIModel)
                  .responseData
                  .userTaskCategorys;
              appbarTitle = listParentUserTaskCategorysModel[0].name;
              wsChildCat(1);
            } catch (e) {}
          }
        }
      } else if (apiState.type == APIType.child_all_cat &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              listChildUserTaskCategorysModel =
                  (model as ChildAllCatAPIModel).responseData.userTaskCategorys;
              setState(() {});
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      listXCardKey = null;
      listParentUserTaskCategorysModel = null;
      listChildUserTaskCategorysModel = null;
    } catch (e) {}
    try {
      _scrollController.dispose();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      APIViewModel().req<ParentAllCatAPIModel>(
        context: context,
        apiState: APIState(APIType.parent_all_cat, this.runtimeType, null),
        url: APIPostTaskCfg.PARENT_TASK_CAT_GET_URL,
        param: {"parentId": 0},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  wsChildCat(int parentId) {
    try {
      APIViewModel().req<ChildAllCatAPIModel>(
        context: context,
        apiState: APIState(APIType.child_all_cat, this.runtimeType, null),
        url: APIPostTaskCfg.CHILD_TASK_CAT_GET_URL,
        param: {"parentId": parentId},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: appbarTitle),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    indeXCardKey = 0;
    listXCardKey = [];

    Future.delayed(Duration.zero, () {
      try {
        if (_scrollController.hasClients) {
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
        }
      } catch (e) {}
    });

    return (listChildUserTaskCategorysModel != null)
        ? Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: getWP(context, 35),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        for (var item in listParentUserTaskCategorysModel)
                          GestureDetector(
                            onTap: () {
                              parentId = item.id;
                              appbarTitle = item.name;
                              wsChildCat(item.id);
                            },
                            child: Container(
                              height: getHP(context, 15),
                              child: Card(
                                elevation: 1,
                                color: parentId == item.id
                                    ? MyTheme.greyColor
                                    : Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Center(
                                    child: Txt(
                                        txt: item.name,
                                        txtColor: Colors.black87,
                                        txtSize: MyTheme.txtSize - .2,
                                        txtAlign: TextAlign.center,
                                        isBold: true),
                                  ),
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    width: getWP(context, 65),
                    child: ListView(
                      controller: _scrollController,
                      shrinkWrap: true,
                      children: [
                        for (var item in listChildUserTaskCategorysModel)
                          drawDropDownBox(item),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        : SizedBox();
  }

  drawDropDownBox(ChildUserTaskCategorysModel childModel) {
    try {
      listXCardKey.add(GlobalKey());
      final GlobalKey<ExpansionTileCardState> cardKey =
          listXCardKey[indeXCardKey];
      indeXCardKey++;

      return Padding(
        padding: const EdgeInsets.all(2),
        child: ExpansionTileCard(
          //  https://pub.dev/packages/expansion_tile_card/example
          initiallyExpanded: true,
          elevation: 0,
          baseColor: MyTheme.brandColor,
          expandedColor: MyTheme.brandColor,
          key: cardKey,
          //leading: CircleAvatar(child: Text('A')),
          title: Txt(
              txt: childModel.name,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false),
          //subtitle: Text('I expand!'),
          children: <Widget>[
            Container(
              color: Colors.white,
              child: drawExpandedChildGrid(
                  childModel.userTaskItemResponseModelList),
            ),
          ],
        ),
      );
    } catch (e) {
      return SizedBox();
    }
  }

  drawExpandedChildGrid(List<UserTaskItemResponseModelList> childList) {
    var _crossAxisSpacing = 10;
    var _screenWidth = getWP(context, 60);
    var _crossAxisCount = 2;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) /
        _crossAxisCount;
    var cellHeight = getHP(context, 10);
    var _aspectRatio = _width / cellHeight;

    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      //padding: const EdgeInsets.only(top: 40, bottom: 40),
      scrollDirection: Axis.vertical,
      itemCount: childList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: _aspectRatio),
      itemBuilder: (context, index) {
        UserTaskItemResponseModelList model = childList[index];

        return GestureDetector(
          onTap: () async {
            if (model.isTaskItemForm) {
              Get.to(() => AddTaskFormScreen(model: model)).then((value) {
                if (value) {
                  Get.back(result: "add_form");
                }
              });
            } else {
              Get.back(result: model.name);
            }
          },
          child: Card(
            elevation: .5,
            color: MyTheme.greyColor,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Txt(
                    txt: model.name,
                    txtColor: MyTheme.timelineTitleColor,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.center,
                    maxLines: 2,
                    txtLineSpace: 1.2,
                    isBold: false),
              ),
            ),
          ),
        );
      },
    );
  }
}
