import 'dart:developer';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/PostTask1APIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_base.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/stepbar/StepperNum.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/textfield/InputTitleBoxHT.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/AddTask3Controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'addtask_page4.dart';

class AddTask3Screen extends StatefulWidget {
  final String dueDate;
  final UserModel userModel;
  const AddTask3Screen({
    Key key,
    @required this.dueDate,
    @required this.userModel,
  }) : super(key: key);
  @override
  State createState() => _AddTask3ScreenState();
}

const double minEstBudget = 10.0;
const double maxEstBudget = 1000000.0;

class _AddTask3ScreenState extends BaseAddTaskStatefull<AddTask3Screen>
    with APIStateListener, StateListener {
  final estBudget = TextEditingController();
  final priceHr = TextEditingController();
  final totalHr = TextEditingController();

  bool isSwitchFixedPrice = true;
  int switchIndex = 0;
  int totalTaskers = 1;
  var mmPrice;
  final AddTask3Controller addTask3Controller = Get.put(AddTask3Controller());

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("task_post_budget");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.del_task &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              taskController.setTaskModel(null);
              Get.back(result: 0);
            } catch (e) {}
          }
        }
      } else if (apiState.type == APIType.put_task3 &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await wsUserDevice(eventType: 'Task Post - Submit');
              taskController.setTaskModel(model.responseData.task);
              Get.to(() => AddTask4Page(
                    dueDate: widget.dueDate,
                    userModel: widget.userModel,
                  )).then((value) {
                Get.back(result: value);
              });
            } catch (e) {}
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  uploadTaskImages(int pageNo) {}

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      estBudget.dispose();
      priceHr.dispose();
      totalHr.dispose();
    } catch (e) {}

    try {
      taskController.dispose();
      addTask3Controller.dispose();
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      try {
        _apiStateProvider = new APIStateProvider();
        _apiStateProvider.subscribe(this);
      } catch (e) {}

      try {
        _stateProvider = new StateProvider();
        _stateProvider.subscribe(this);
        StateProvider()
            .notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
      } catch (e) {}

      final f = NumberFormat("###,###", "en_US");
      mmPrice = getCurSign() +
          minEstBudget.toStringAsFixed(0) +
          "-" +
          getCurSign() +
          f.format(maxEstBudget);
      final taskModel = taskController.getTaskModel();
      if (taskModel != null) {
        totalTaskers = taskModel.workerNumber > 0 ? taskModel.workerNumber : 1;
        if (taskModel.isFixedPrice) {
          isSwitchFixedPrice = true;
          switchIndex = 0;
          estBudget.text = taskModel.fixedBudgetAmount.toString();
        } else {
          isSwitchFixedPrice = false;
          switchIndex = 1;
          priceHr.text = taskModel.hourlyRate.toString();
          totalHr.text = taskModel.totalHours.toString();
        }
        calculation();
      }
    } catch (e) {}

    try {
      wsUserDevice(eventType: 'Task Post - Task Budget');
    } catch (e) {}
  }

  validate() {
    if (isSwitchFixedPrice) {
      try {
        if (double.parse(estBudget.text) < minEstBudget ||
            double.parse(estBudget.text) > maxEstBudget) {
          showToast(
            context: context,
            msg: 'post_task_price_invalid_price_text'.tr + mmPrice,
          );
          return false;
        }
      } catch (e) {
        showToast(
          context: context,
          msg: 'post_task_price_invalid_price_text'.tr + mmPrice,
        );
        return false;
      }
    } else {
      if (priceHr.text.isEmpty) {
        showToast(
          context: context,
          msg: 'post_task_price_rate_label'.tr + '?',
        );
        return false;
      }
      if (totalHr.text.isEmpty) {
        showToast(
          context: context,
          msg: 'post_task_price_error_hours'.tr,
        );
        return false;
      }
      final amt = (addTask3Controller.totalAmounts.toDouble() * totalTaskers);
      if (amt < minEstBudget || amt > maxEstBudget) {
        showToast(
          context: context,
          msg: 'post_task_price_invalid_price_text'.tr + mmPrice,
        );
        return false;
      }
    }

    return true;
  }

  calculation() {
    addTask3Controller.calculation(
      isSwitchFixedPrice: isSwitchFixedPrice,
      estBudget: estBudget,
      priceHr: priceHr,
      totalHr: totalHr,
    );
  }

  onNextClicked() async {
    if (validate()) {
      var status = taskController.getStatusCode();
      if (status == TaskStatusCfg.TASK_STATUS_DRAFT) {
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
      }

      var mEstBudget =
          estBudget.text == "" ? 0.0 : double.parse(estBudget.text.trim());
      final mPriceHr =
          priceHr.text == "" ? 0.0 : double.parse(priceHr.text.trim());
      final mtotalHr =
          totalHr.text == "" ? 0.0 : double.parse(totalHr.text.trim());

      if (mEstBudget == 0) {
        mEstBudget = mPriceHr;
      }

      APIViewModel().req<PostTask1APIModel>(
        context: context,
        apiState: APIState(APIType.put_task3, this.runtimeType, null),
        url: APIPostTaskCfg.POSTTASK1_PUT_URL,
        reqType: ReqType.Put,
        param: {
          "DeliveryDate": widget.dueDate,
          "DeliveryTime": taskController.getTaskModel().deliveryTime,
          "Description": taskController.getTaskModel().description,
          "DueAmount": taskController.getTaskModel().dueAmount,
          "DutDateType": taskController.getTaskModel().dutDateType,
          "EmployeeId": taskController.getTaskModel().employeeId,
          "FixedBudgetAmount": mEstBudget,
          "HourlyRate": mPriceHr,
          "IsFixedPrice": isSwitchFixedPrice,
          "IsInPersonOrOnline":
              taskController.getTaskModel().isInPersonOrOnline,
          "JobCategory": taskController.getTaskModel().jobCategory,
          "Latitude": taskController.getTaskModel().latitude,
          "Longitude": taskController.getTaskModel().longitude,
          "NetTotalAmount": addTask3Controller.totalAmounts.toDouble(),
          "PaidAmount": taskController.getTaskModel().paidAmount,
          "PreferedLocation":
              taskController.getTaskModel().preferedLocation ?? '',
          "Requirements": taskController.getTaskModel().requirements,
          "Skill": taskController.getTaskModel().skill,
          "Status": status,
          "Id": taskController.getTaskModel().id,
          "Title": taskController.getTaskModel().title,
          "TotalBidsNumber": taskController.getTaskModel().totalBidsNumber,
          "TotalHours": mtotalHr,
          "UserId": taskController.getTaskModel().userId,
          "WorkerNumber": totalTaskers,
        },
      );
    }
  }

  onDelTaskClicked() {
    try {
      APIViewModel().req<DelTaskAPIModel>(
        context: context,
        apiState: APIState(APIType.del_task, this.runtimeType, null),
        url: APIPostTaskCfg.DEL_TASK_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: drawAppbar(
          pageNo: 3,
          userModel: widget.userModel,
          title: 'post_task_screen_title_new'.tr,
          pos: 2,
          isBold1: true,
          isBold2: true,
          isBold3: true,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "continue".tr,
            //icon: Icons.arrow_forward,
            callback: () async {
              onNextClicked();
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(height: 20),
            Center(
              child: ToggleSwitch(
                minWidth: getWP(context, 30),
                minHeight: getHP(context, MyTheme.switchBtnHpa),
                initialLabelIndex: switchIndex,
                cornerRadius: 50.0,
                fontSize: isLng(AppDefine.GEO_CODE) ? 14 : 17,
                activeBgColor: MyTheme.brandColor,
                activeFgColor: Colors.white,
                inactiveBgColor: MyTheme.gray3Color,
                inactiveFgColor: Colors.white,
                labels: [
                  'post_task_price_state_label_total'.tr,
                  'post_task_price_state_label_hourly_rate'.tr
                ],
                //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
                onToggle: (index) {
                  switchIndex = index;
                  isSwitchFixedPrice = (index == 0) ? true : false;
                  calculation();
                  setState(() {});
                },
              ),
            ),

            /*Center(
              child: SwitchView(
                onTxt: "Total",
                offTxt: "Hourly rate",
                value: isSwitchFixedPrice,
                onChanged: (value) {
                  isSwitchFixedPrice = value;
                  if (mounted) {
                    setState(() {});
                  }
                  //callback((isSwitch) ? _companyName.text.trim() : '');
                },
              ),
            ),*/
            SizedBox(height: 40),
            (isSwitchFixedPrice) ? drawEstBudgetView() : drawPriceHrView(),

            //drawEstBudgetView(),
            // drawPriceHrView(),
            SizedBox(height: 40),
            drawTaskerNosView(),
          ],
        ),
      ),
    );
  }

  drawEstBudgetView() {
    if (!mounted) return;
    if (estBudget.text == "0.0") {
      estBudget.text = "";
    }

    return InputTitleBoxHT(
      title: "post_task_price_price_label".tr + mmPrice + ")",
      ph: "00.00",
      input: estBudget,
      kbType: TextInputType.number,
      len: 7,
      minLen: 0,
      prefixIco: Text(
        getCurSign(),
        style: TextStyle(color: Colors.black, fontSize: 25),
      ),
      onChange: calculation(),
    );
  }

  drawPriceHrView() {
    if (!mounted) return;
    return Padding(
      padding: const EdgeInsets.only(top: 22),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Container(
                child: InputTitleBoxHT(
                  title: "post_task_price_rate_label".tr,
                  ph: "0.0",
                  input: priceHr,
                  kbType: TextInputType.number,
                  len: 7,
                  minLen: 0,
                  prefixIco: Text(getCurSign(),
                      style: TextStyle(color: Colors.black, fontSize: 25)),
                  onChange: calculation(),
                ),
              ),
            ),
            SizedBox(width: 20),
            Flexible(
              child: Container(
                child: InputTitleBoxHT(
                  title: "post_task_price_hours_label".tr,
                  ph: "0",
                  input: totalHr,
                  kbType: TextInputType.number,
                  len: 2,
                  minLen: 0,
                  onChange: calculation(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawTaskerNosView() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 6,
                child: Txt(
                    txt: "taskers_need".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(width: 10),
              Flexible(
                //flex: 2,
                child: Txt(
                    txt: totalTaskers.toString(),
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(width: 10),
              Flexible(
                flex: 3,
                child: StepperNum(
                  count: totalTaskers,
                  callback: (val) {
                    totalTaskers = val;
                    calculation();
                    setState(() {});
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Txt(
                    txt: "post_task_price_estimated_budget_label".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              //SizedBox(width: getWP(context, 23)),
              Expanded(
                //flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Obx(
                      () => Txt(
                          txt: (totalTaskers == 1)
                              ? (getCurSign() +
                                  addTask3Controller.totalAmounts
                                      .toStringAsFixed(2))
                              : (getCurSign() +
                                  addTask3Controller.totalAmounts
                                      .round()
                                      .toString()),
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize + .5,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Txt(
                            txt: (addTask3Controller.totalAmounts.toDouble() >
                                        0 &&
                                    totalTaskers > 1)
                                ? 'post_task_price_per_tasker_label'.tr
                                : '',
                            txtColor: MyTheme.gray5Color,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
