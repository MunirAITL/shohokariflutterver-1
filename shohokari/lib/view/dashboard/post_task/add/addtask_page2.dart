import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_base.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page3.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AddTask2Page extends StatefulWidget {
  final UserModel userModel;
  const AddTask2Page({
    Key key,
    @required this.userModel,
  }) : super(key: key);
  @override
  State createState() => _AddTask2PageState();
}

class _AddTask2PageState extends BaseAddTaskStatefull<AddTask2Page>
    with APIStateListener, StateListener {
  String dueDate = "";

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("task_post_due_date");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.del_task &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              taskController.setTaskModel(null);
              Get.back(result: 0);
            } catch (e) {}
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  uploadTaskImages(int pageNo) {}

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      taskController.dispose();
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
    try {
      if (taskController.getTaskModel().id != null) {
        dueDate = taskController.getTaskModel().deliveryDate;
        final f1 = new DateFormat('dd-MMM-yyyy');
        final f2 = new DateFormat(DateFun.getDateBDFormat());
        final d = f1.parse(dueDate);
        dueDate = f2.format(d);
        log(dueDate);
      } else {
        final date = new DateTime.now();
        dueDate = DateFun.getFormatedDate(
            format: DateFun.getDateBDFormat(),
            mDate: DateTime(date.year, date.month, date.day));
      }
    } catch (e) {}
    try {
      wsUserDevice(eventType: 'Task Post - Task Date');
    } catch (e) {}
  }

  onNextClicked() {
    Get.to(
      () => AddTask3Screen(
        dueDate: dueDate,
        userModel: widget.userModel,
      ),
    ).then(
      (value) {
        if (value == 0 || value == 4) Get.back(result: value);
      },
    );
  }

  onDelTaskClicked() {
    try {
      APIViewModel().req<DelTaskAPIModel>(
        context: context,
        apiState: APIState(APIType.del_task, this.runtimeType, null),
        url: APIPostTaskCfg.DEL_TASK_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //log(taskController.getTaskModel().id.toString());
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: drawAppbar(
          pageNo: 2,
          userModel: widget.userModel,
          title: 'post_task_screen_title_new'.tr,
          pos: 1,
          isBold1: true,
          isBold2: true,
          isBold3: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "continue".tr,
            //icon: Icons.arrow_forward,
            callback: () async {
              onNextClicked();
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 1, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month, dateNow.day);

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            DatePickerView(
              cap: 'post_task_schedule_due_date_when_do_you_label'.tr,
              isCapBold: false,
              capTxtColor: MyTheme.gray5Color,
              txtColor: Colors.black,
              topbotHeight: 10,
              borderWidth: 0,
              dt: (dueDate == '')
                  ? 'propose_new_time_due_date_label'.tr
                  : dueDate,
              initialDate: dateExpfirst,
              firstDate: dateExpfirst,
              lastDate: dateExplast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      dueDate =
                          DateFormat('dd-MMM-yyyy').format(value).toString();
                    } catch (e) {
                      log(e.toString());
                    }
                  });
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
