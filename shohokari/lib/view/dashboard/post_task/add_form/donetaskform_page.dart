import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/taskform/SubmitTaskFormAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DoneTaskPage extends StatefulWidget {
  final SubmitTaskFormAPIModel model;
  const DoneTaskPage({Key key, @required this.model}) : super(key: key);
  @override
  State createState() => _DoneTaskPageState();
}

const minEstBudget = 50;
const maxEstBudget = 1000000;

class _DoneTaskPageState extends State<DoneTaskPage>
    with Mixin, APIStateListener {
  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      } else if (apiState.type == APIType.save_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      //  email notification api call
      /*await APIViewModel().req<CommonAPIModel>(
        context: context,
        apiState: APIState(APIType.email_noti, this.runtimeType, null),
        url: APIEmailNotiCfg.TASK_EMAI_NOTI_GET_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        isLoading: false,
        reqType: ReqType.Get,
      );*/
    } catch (e) {}
  }

  onNextClicked() {}
  onDelTaskClicked() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          automaticallyImplyLeading: false,
          title: UIHelper()
              .drawAppbarTitle(title: 'offer_accepted_confirmation_title'.tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "done".tr,
            callback: () async {
              Get.back(result: true);
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Txt(
                txt: "offer_successful_title"
                    .tr, //+ widget.userModel.firstName + ",",
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize + 1.3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 20),
              Txt(
                txt: "post_task_finished_your_task_was_posted_text".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              )
            ],
          ),
        ),
      ),
    );
  }
}
