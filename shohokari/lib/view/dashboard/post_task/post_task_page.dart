import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APIYoutubeCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/dashboard/post_task/all_cat/all_cat_page.dart';
import 'package:aitl/view/widgets/btn/IconBtnQ.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/grid/SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view_model/helper/utils/PostTaskHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:launch_review/launch_review.dart';

import '../dashboard_base.dart';
import 'confirm_your_offer/confirm_your_offer_page.dart';

class PostTaskListPage extends StatefulWidget {
  @override
  State createState() => PostTaskListPageState();
}

class PostTaskListPageState extends BaseDashboard<PostTaskListPage>
    with Mixin, UIHelper, StateListener {
  //  **************  app states start

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        refreshData();
      } else if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("task_post_category");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  Future<void> refreshData() async {
    try {
      if (mounted) {}
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: UIHelper()
              .drawAppbarTitle(title: 'navigation_bar_post_task_label'.tr),
          centerTitle: false,
          actions: drawSoundLngBox(this.runtimeType, audioController, [
            IconBtnQ(
                tag: 1,
                icon: Icon(Icons.help, color: Colors.white),
                onPressed: () {
                  openUrl(context, APIYoutubeCfg.HELP_POSTER_YOUTUBE_URL);
                })
          ]),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(AppConfig.post_add_height),
            child: Container(
              color: MyTheme.grayColor,
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: Txt(
                    txt: "tutorial_text_task_post".tr,
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    isOverflow: true,
                    isBold: false),
              ),
            ),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawBannerGrid(),
            drawTaskList(),
            Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 30),
                child: drawHireWorkButtons()),
          ],
        ),
      ),
    );
  }

  drawBannerGrid() {
    //double h = 30;
    return Container(
        child: CarouselSlider(
      options: CarouselOptions(
        height: getHP(context, 23),
        aspectRatio: 16 / 9,
        viewportFraction: 1,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 5),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        //enlargeCenterPage: true,
        //onPageChanged: callbackFunction,
        scrollDirection: Axis.horizontal,
      ),
      items: APIPostTaskCfg.listBannerUrl
          .asMap()
          .map((i, element) => MapEntry(
              i,
              GestureDetector(
                onTap: () {
                  switch (i) {
                    case 0:
                      Get.to(
                        () => AllCatPage(),
                      ).then((value) {
                        if (value != null) {
                          if (value == "add_form") {
                            StateProvider().notify(ObserverState.STATE_OPEN_TAB,
                                DashboardPage.TAB_FINDWORKS);
                          } else {
                            Get.to(
                              () => AddTask1Screen(
                                index: null,
                                userModel: userData.userModel,
                                categoryPH: value,
                              ),
                            ).then((pageNo) {
                              //obsUpdateTabs(pageNo);
                            });
                          }
                        }
                      });
                      break;
                    case 1:
                      if (userData.communityId != 2) {
                        Get.to(() => ConfirmYourOfferPage());
                      } else {
                        //StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
                        //DashboardPage.TAB_FINDWORKS);
                      }
                      break;
                    case 2:
                      Get.to(() => AddTask1Screen(
                            userModel: userData.userModel,
                            categoryPH: 'suggestion_example_security_gaurd'.tr,
                          ));
                      break;
                    default:
                  }
                },
                child: Container(
                  //width: getW(context),
                  //margin: EdgeInsets.symmetric(horizontal: 0),
                  //margin: EdgeInsets.symmetric(horizontal: 0),
                  child: Stack(
                    children: <Widget>[
                      Positioned.fill(
                        child: MyNetworkImage().loadCacheImage(
                          url: element.toString(),
                        ),
                      ),
                      //Image.network(element, fit: BoxFit.cover)),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromARGB(200, 0, 0, 0),
                                  Color.fromARGB(0, 0, 0, 0)
                                ],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                              ),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 20.0),
                            child: new DotsIndicator(
                              dotsCount: APIPostTaskCfg.listBannerUrl.length,
                              position: double.parse(i.toString()),
                              decorator: DotsDecorator(
                                color: Colors.black, // Inactive color
                                activeColor: Colors.white,
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
              )))
          .values
          .toList(),
    ));
  }

  drawTaskList() {
    final w = getW(context) / 4;
    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      //padding: const EdgeInsets.only(top: 40, bottom: 40),
      scrollDirection: Axis.vertical,
      itemCount: PostTaskHelper().listTask.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
        crossAxisCount: 3,
        crossAxisSpacing: 0,
        mainAxisSpacing: 0,
        height: getHP(context, 22),
      ),
      itemBuilder: (context, index) {
        final map = PostTaskHelper().listTask[index];
        final icon = map["icon"];
        final title = map["title"];
        final isCat = map['isCat'];

        return GestureDetector(
          onTap: () async {
            if (!isCat) {
              final playstore_appId = map['playstore_appId'].toString();
              final appstore_appId = map['appstore_appId'].toString();
              if (playstore_appId.isNotEmpty || appstore_appId.isNotEmpty) {
                LaunchReview.launch(
                    androidAppId: playstore_appId, iOSAppId: appstore_appId);
              } else {
                Get.to(
                  () => AddTask1Screen(
                    index: index,
                    userModel: userData.userModel,
                  ),
                ).then((pageNo) {
                  //obsUpdateTabs(pageNo);
                });
              }
            } else {
              Get.to(
                () => AllCatPage(),
              ).then((value) {
                if (value != null) {
                  if (value == "add_form") {
                    StateProvider().notify(ObserverState.STATE_OPEN_TAB,
                        DashboardPage.TAB_FINDWORKS);
                  } else {
                    Get.to(
                      () => AddTask1Screen(
                        index: null,
                        userModel: userData.userModel,
                        categoryPH: value,
                      ),
                    ).then((pageNo) {
                      //obsUpdateTabs(pageNo);
                    });
                  }
                }
              });
            }
          },
          child: Container(
            //elevation: 0,
            //color: MyTheme.bgColor,
            margin: new EdgeInsets.all(10),
            //color: Colors.black,
            //width: w,
            // height: w + 50,
            child: new Column(
              //mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: w - 15,
                  height: w - 15,
                  //color: Colors.black,
                  child: SvgPicture.asset(
                    icon,
                    //width: w,
                    //height: w,
                    //fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 5),
                Expanded(
                  flex: 2,
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: MyTheme.gray5Color, //MyTheme.iconTextColor,
                      fontSize: (title.length > 25) ? 11 : 14,
                    ),
                  ),
                ),
                //just for testing, will fill with image later
              ],
            ),
          ),
        );
      },
    );
  }

  drawHireWorkButtons() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Flexible(
            child: MMBtn(
              txt: "task_post_i_am_worker".tr,
              //txtColor: Colors.white,
              //bgColor: accentColor,
              width: getWP(context, 48),
              height: getHP(context, MyTheme.btnHpa - 1),
              radius: 20,
              callback: () {
                if (userData.communityId != 2) {
                  Get.to(() => ConfirmYourOfferPage());
                } else {
                  //StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
                  // DashboardPage.TAB_FINDWORKS);
                }
              },
            ),
          ),
          //SizedBox(width: 10),
          Flexible(
            child: MMBtn(
              txt: "task_post_i_need_employee".tr,
              //txtColor: Colors.white,
              //bgColor: accentColor,
              width: getWP(context, 48),
              height: getHP(context, MyTheme.btnHpa - 1),
              radius: 20,
              callback: () {
                Get.to(() => AddTask1Screen(
                      userModel: userData.userModel,
                      categoryPH: 'suggestion_example_anything_else'.tr,
                    ));
              },
            ),
          ),
        ],
      ),
    );
  }
}
