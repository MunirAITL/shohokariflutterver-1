import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:aitl/data/model/dashboard/user/PublicUserModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/pubnub/PubnubMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:intl/intl.dart';
import 'chat_base.dart';

class ChatPage extends StatefulWidget {
  final bool isSupport;
  final bool isPublicChat;
  final String description;
  const ChatPage({Key key, this.isSupport, this.isPublicChat, this.description})
      : super(key: key);
  @override
  State createState() => _ChatPageState();
}

class _ChatPageState extends BaseChatStatefull<ChatPage> with APIStateListener {
  final TextEditingController textController = TextEditingController();
  ScrollController scrollController = new ScrollController();

  List<TimelinePostModel> listTimeLineModel = [];

  PubnubMgr pubnubMgr;

  //Timer timer;
  //static const int callTimelineSec = 10;
  //  page stuff start here
  bool isPageDone = false;
  bool isScrolling = false;
  bool isPosting = false;
  int pageStart = 1;
  int pageCount = AppConfig.page_limit;
  String msgTmp = '';

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_timeline_by_app &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final listTimeLineModel2 = (model as GetTimeLineByAppAPIModel)
                  .responseData
                  .timelinePosts;
              if (listTimeLineModel2.length > 0 &&
                  listTimeLineModel2.length != listTimeLineModel.length) {
                listTimeLineModel = listTimeLineModel2;
                setState(() {
                  if (listTimeLineModel.length < pageCount) {
                    pageStart = 1;
                    isPageDone = true;
                  }
                  if (!isScrolling) {
                    Future.delayed(const Duration(seconds: 1), () {
                      scrollDown(scrollController,
                          getH(context) * AppConfig.chatScrollHeight);
                    });
                  }
                });
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostTimeline((model as MediaUploadFilesAPIModel));
          }
        }
      } else if (apiState.type == APIType.post_timeline &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final postModel = (model as TimelinePostAPIModel).responseData.post;
            final timelineId = postModel.id;

            try {
              final user = new User(
                  id: userData.userModel.id, name: userData.userModel.name);
              final chatModel =
                  new ChatModel(timelinePostModel: postModel, user: user);
              pubnubMgr.postMessage(
                  chatModel: chatModel, receiverId: postModel.receiverId);
            } catch (e) {}

            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TIMELINE_EMAI_NOTI_GET_URL
                  .replaceAll("#timelineId#", timelineId.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  wsPostTimeline(MediaUploadFilesAPIModel model) async {
    try {
      //final jsonRes = json.encode(model.jsonRes);
      isPosting = true;
      var res = '';
      try {
        res = model.jsonRes.toString();
      } catch (e) {}
      final param = {
        "AdditionalAttributeValue": res ?? '',
        "Checkin": "",
        "FromLat": 0.0,
        "FromLng": 0.0,
        "IsPrivate": (widget.isSupport != null)
            ? false
            : (widget.isPublicChat == null)
                ? true
                : false,
        "Message": msgTmp,
        "OwnerId": userData.userModel.id,
        "PostTypeName": "status", //(url == null) ? "status" : "picture",
        "TaskId":
            (widget.isSupport != null) ? 0 : myTaskController.getTaskModel().id,
      };
      log(json.encode(param));
      msgTmp = '';
      await APIViewModel().req<TimelinePostAPIModel>(
        context: context,
        apiState: APIState(APIType.post_timeline, this.runtimeType, null),
        url: APITimelineCfg.TIMELINE_POST_URL,
        isLoading: false,
        reqType: ReqType.Post,
        param: param,
      );
      isPosting = false;
    } catch (e) {}
  }

  wsGetTimelineByAPI() async {
    try {
      // userId, taskId. timeLineId, IsPrivate,Page,Count
      await APIViewModel().req<GetTimeLineByAppAPIModel>(
        context: context,
        apiState: APIState(APIType.get_timeline_by_app, this.runtimeType, null),
        url: APITimelineCfg.GET_TIMELINE_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": (widget.isPublicChat == null) ? pageCount : 100,
          "CustomerId": (widget.isSupport != null) ? userData.userModel.id : 0,
          "IsPrivate": (widget.isSupport != null)
              ? false
              : (widget.isPublicChat == null)
                  ? true
                  : false,
          "Page": pageStart,
          "TaskId": (widget.isSupport != null)
              ? 0
              : myTaskController.getTaskModel().id,
          "timeLineId": 0,
        },
      );
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimeLineModel = null;
    textController.dispose();
    scrollController.dispose();
    scrollController = null;

    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      pubnubMgr.destroy();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      try {
        _apiStateProvider = new APIStateProvider();
        _apiStateProvider.subscribe(this);
      } catch (e) {}
      await wsGetTimelineByAPI();
      pubnubMgr = PubnubMgr();
      await pubnubMgr.initPubNub(callback: (envelope) async {
        log(envelope.payload);
        if (!isPosting) {
          pageStart = 1;
          await wsGetTimelineByAPI();
        }
      });
      //await pubnubMgr.getHistory(receiverId: 114453);
    } catch (e) {
      log(e.toString());
    }
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            isScrolling = true;
            if (!isPageDone && msgTmp == '') {
              pageStart++;
              wsGetTimelineByAPI();
            }
          } else if (scrollNotification is ScrollEndNotification) {
            isScrolling = false;
          }
          return true;
        },
        child: ListView.builder(
          controller: scrollController,
          //padding: new EdgeInsets.all(8.0),
          reverse: false,
          //shrinkWrap: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            return buildSingleMessage(listTimeLineModel, index, false);
          },
        ),
      ),
    );
  }

  Widget buildQList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            isScrolling = true;
            if (!isPageDone && msgTmp == '') {
              pageStart++;
              wsGetTimelineByAPI();
            }
          } else if (scrollNotification is ScrollEndNotification) {
            isScrolling = false;
          }
          return true;
        },
        child: ListView.builder(
          controller: scrollController,
          //padding: new EdgeInsets.all(8.0),
          reverse: false,
          //shrinkWrap: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            final list = listTimeLineModel[index].userCommentPublicModelList;
            if (list.length > 0)
              return Column(
                children: [
                  buildSingleMessage(listTimeLineModel, index, true),
                  Padding(
                    padding: const EdgeInsets.only(left: 50),
                    child: buildQMessage(list[list.length - 1], index),
                  ),
                ],
              );
            else
              return SizedBox();
          },
        ),
      ),
    );
  }

  _chatTextArea() {
    if (myTaskController == null) return SizedBox();
    if (myTaskController.getStatusCode() ==
            TaskStatusCfg.TASK_STATUS_PAYMENTED &&
        widget.isSupport == null) {
      return Container(
          color: MyTheme.hotdipPink.withAlpha(60),
          width: getW(context),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt: "private_conversation_messages_closed_message".tr,
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ));
    } else if (myTaskController.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_CANCELLED) {
      return Container(
          color: MyTheme.hotdipPink.withAlpha(60),
          width: getW(context),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt: "private_conversation_messages_cancel_message".tr,
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ));
      ;
    } else {
      TimelinePostModel timelineModel;
      try {
        timelineModel = listTimeLineModel[index2];
      } catch (e) {}
      return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: drawMessageBox(
          context: context,
          picUrl: null,
          hintText: 'hint_enter_a_message'.tr,
          iconLeft: Icon(
            Icons.add,
            color: Colors.green,
          ),
          iconRight: Icon(
            Icons.send,
            color: Colors.white,
          ),
          textController: textController,
          callbackCam: (File file, String txt) async {
            textController.clear();
            msgTmp = txt.trim();
            if (file != null) {
              await APIViewModel().upload(
                context: context,
                apiState:
                    APIState(APIType.media_upload_file, this.runtimeType, null),
                file: file,
              );
            } else {
              FocusScope.of(context).requestFocus(FocusNode());
              if (txt.trim().isNotEmpty) {
                //Add the message to the list
                if (timelineModel != null) {
                  String formattedDate =
                      DateFormat('dd-MMM-yyyy').format(DateTime.now());
                  final TimelinePostModel timeLinePostModel =
                      TimelinePostModel();

                  timeLinePostModel.ownerId = timelineModel.ownerId;
                  timeLinePostModel.ownerImageUrl = timelineModel.ownerImageUrl;
                  timeLinePostModel.additionalAttributeValue = '';
                  timeLinePostModel.message = msgTmp;
                  timeLinePostModel.dateCreatedUtc = formattedDate;
                  listTimeLineModel.add(timeLinePostModel);
                  scrollDown(scrollController,
                      getH(context) * AppConfig.chatScrollHeight);
                }
                textController.clear();
                setState(() {});
                //pageStart = 1;
                //pubnubMgr.postMessage();
                Future.delayed(Duration(seconds: 1), () async {
                  pageStart = 1;
                  wsPostTimeline(null);
                });
              }
            }
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(
              title: widget.isPublicChat != null
                  ? 'questions'.tr
                  : ('private_messages'.tr +
                      ((myTaskController.getTaskModel().title != null &&
                              widget.isSupport == null)
                          ? " : " + myTaskController.getTaskModel().title
                          : ''))),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          widget.isPublicChat != null ? buildQList() : buildMessageList(),
          //Divider(height: 1.0),
          _chatTextArea(),
        ],
      ),
    );
  }
}
