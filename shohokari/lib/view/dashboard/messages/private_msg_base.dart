import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/messages/chat_page.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BasePrivateMsgStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TimeLineHelper {
  final myTaskController = Get.put(MyTaskController());

  bool isLoading = false;

  refreshData();

  drawSearchbar(TextEditingController searchText, Function(String) onChange) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextField(
        controller: searchText,
        autofocus: true,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.search,
        onChanged: (value) => onChange(value),
        autocorrect: false,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
        decoration: InputDecoration(
          counter: Offstage(),
          prefixIcon: Icon(
            Icons.search,
            color: Colors.white,
          ),
          suffixIcon: IconButton(
              onPressed: () => onChange(""),
              icon: Icon(
                Icons.close,
                color: Colors.white,
              )),
          hintText: "search_by_title".tr,
          hintStyle: new TextStyle(
            color: Colors.white60,
            fontSize: 20,
            //height: MyTheme.txtLineSpace,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }

  drawAppbarNavBar(Function callback) {
    return PreferredSize(
      preferredSize: Size.fromHeight(AppConfig.private_msg_height),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Get.to(() => ChatPage(isSupport: true));
              },
              child: Container(
                color: MyTheme.l2greyColor,
                child: Padding(
                  padding: const EdgeInsets.only(left: 5, top: 10, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        child: Image.asset(
                          'assets/images/logo/logo.png',
                          width: 50,
                          height: 50,
                        ),
                      ),
                      SizedBox(width: 12),
                      Center(
                        child: Txt(
                            txt: "message_shohokari_support_team_contact".tr,
                            txtColor: MyTheme.gray5Color,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : Container(
                    height: .5,
                    color: MyTheme.bgColor,
                  )
          ],
        ),
      ),
    );
  }

  drawItem(TaskModel taskModel) {
    final priceTxt = (taskModel.isFixedPrice)
        ? taskModel.fixedBudgetAmount.round().toString()
        : taskModel.hourlyRate.round().toString() + '/hr';
    //
    var status = taskModel.status;
    try {
      if (int.parse(status) > 0) {
        status = TaskStatusCfg().getSatus(int.parse(status));
      }
    } catch (e) {
      status = taskModel.status;
    }

    return GestureDetector(
      onTap: () async {
        await APIViewModel().req<GetTaskAPIModel>(
          context: context,
          url: APIMyTasksCfg.GET_TASK_URL
              .replaceAll("#taskId#", taskModel.id.toString()),
          reqType: ReqType.Get,
          callback: (model) async {
            if (model != null && mounted) {
              if (model.success) {
                final taskModel = model.responseData.task;
                if (taskModel != null) {
                  await myTaskController.setTaskModel(taskModel);
                  Get.to(() => ChatPage()).then((value) {
                    myTaskController.setTaskModel(null);
                  });
                }
              }
            }
          },
        );
      },
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: getWP(context, 10),
                    height: getWP(context, 10),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: MyNetworkImage.loadProfileImage(
                            taskModel.ownerImageUrl),
                        fit: BoxFit.cover,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Txt(
                        txt: taskModel.title,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Txt(
                          txt: TaskStatusCfg().getSatusLng(status),
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .6,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      Container(
                        //color: Colors.black,
                        child: drawPrice(
                          price: priceTxt,
                          txtSize: 18,
                          txtColor: MyTheme.gray5Color,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  SizedBox(width: getWP(context, 12)),
                  Expanded(
                    child: Container(
                      color: MyTheme.gray4Color,
                      height: .3,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  drawNF() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          //shrinkWrap: true,
          children: [
            Container(
              width: getWP(context, 80),
              height: getWP(context, 50),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_msg.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt: "msg_nf".tr,
                  txtColor: MyTheme.mycasesNFBtnColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
            SizedBox(height: 10),
            Btn(
                txt: "refresh".tr,
                txtColor: Colors.white,
                bgColor: MyTheme.brandColor,
                txtSize: 1.8,
                radius: 0,
                callback: () {
                  refreshData();
                }),
          ],
        ),
      ),
    );
  }
}
