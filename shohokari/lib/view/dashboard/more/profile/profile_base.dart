import 'dart:io';

import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/cfg/AppShareCfg.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgesModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortfulioModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/profile/edit_profile_page.dart';
import 'package:aitl/view/dashboard/more/resolutions/res_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/ProfileHelper.dart';
import 'package:aitl/view_model/helper/utils/TaskHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:aitl/view_model/rx/ProfileController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:share/share.dart';

abstract class BaseProfileStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  var myTaskController = Get.put(MyTaskController());
  final profileController = Get.put(ProfileController());

  //  expandable bg
  double h1 = 50;
  double h2 = 60;

  bool isLoading = false;
  bool isPublicUser = true;
  int posterSwitchValue = 0;

  refreshData();

  drawHeader(UserModel userModel, cls) {
    if (userModel == null) {
      return Container(color: Colors.white);
    }

    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            elevation: 0,
            backgroundColor: MyTheme.bgColor,
            iconTheme: IconThemeData(
                color: (innerBoxIsScrolled)
                    ? Colors.black
                    : Colors.white //change your color here
                ),
            title: UIHelper().drawAppbarTitle(
                title: 'profile'.tr,
                txtColor: (innerBoxIsScrolled) ? Colors.black : Colors.white),
            centerTitle: false,
            actions: [
              !isPublicUser
                  ? IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        Get.to(() => EditProfilePage()).then((value) {
                          userModel = userData.userModel;
                          refreshData();
                        });
                      })
                  : SizedBox(),
              SizedBox(width: 30),
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () async {
                  final msg = AppShareCfg.SERVER_SHARE_PUBLIC_URL +
                      userData.userModel.userProfileUrl;
                  await Share.share(msg);
                },
              ),
              SizedBox(width: 10),
            ],

            expandedHeight: getHP(context, !isPublicUser ? h1 : h2),
            floating: false,
            pinned: true,
            snap: false,
            forceElevated: true,
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.parallax,
              centerTitle: true,
              background: drawUserProfileData(userModel, cls),
            ),
            //background:
          ),
        ];
      },
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout()),
    );
  }

  drawUserProfileData(UserModel userModel, cls) {
    final dt = DateFun.getTimeAgoTxt(userModel.dateCreatedLocal);
    final address = Common.removeTag(userModel.address);
    return Container(
      //height: getHP(context, !isPublicUser ? h1 : h2),
      decoration: MyNetworkImage.isValidUrl(userModel.coverImageUrl)
          ? BoxDecoration(
              color: Colors.black,
              image: DecorationImage(
                image: MyNetworkImage.loadProfileImage(userModel.coverImageUrl),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.7), BlendMode.dstATop),
                fit: BoxFit.cover,
              ),
            )
          : BoxDecoration(color: MyTheme.airBlueColor.withOpacity(.7)),
      child: ListView(
        primary: false,
        children: [
          Container(
            width: double.infinity,
            child: Column(
              //shrinkWrap: true,
              //primary: false,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                !isPublicUser
                    ? Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            CamPicker().showCamDialog(
                              context: context,
                              isRear: true,
                              callback: (File path) {
                                if (path != null) {
                                  APIViewModel().upload(
                                    context: context,
                                    apiState: APIState(
                                        APIType.media_profile_cover_image,
                                        cls,
                                        null),
                                    file: path,
                                  );
                                }
                              },
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 70, right: 20),
                            child: Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: MyTheme.gray1Color),
                                child: Icon(
                                  Icons.camera_alt_outlined,
                                  size: 20,
                                  color: Colors.pink,
                                )),
                          ),
                        ),
                      )
                    : SizedBox(),
                SizedBox(height: !isPublicUser ? 30 : 60),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Stack(
                      //alignment: Alignment.center,
                      children: [
                        Container(
                          width: getWP(context, 25),
                          height: getWP(context, 25),
                          child: Container(
                            height: getW(context) * 0.3,
                            width: getW(context) * 0.3,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: MyNetworkImage.loadProfileImage(
                                    userModel.profileImageUrl),
                                fit: BoxFit.cover,
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                        ),
                        !isPublicUser
                            ? Positioned(
                                right: 0,
                                //left: 20,
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  child: MaterialButton(
                                    onPressed: () {
                                      CamPicker().showCamDialog(
                                        context: context,
                                        isRear: false,
                                        callback: (File path) {
                                          if (path != null) {
                                            APIViewModel().upload(
                                              context: context,
                                              apiState: APIState(
                                                  APIType.media_profile_image,
                                                  cls,
                                                  null),
                                              file: path,
                                            );
                                          }
                                        },
                                      );
                                    },
                                    color: MyTheme.gray1Color,
                                    child: Icon(
                                      Icons.camera_alt_outlined,
                                      size: 20,
                                      color: Colors.pink,
                                    ),
                                    padding: EdgeInsets.all(0),
                                    shape: CircleBorder(),
                                  ),
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 7),
                      child: Txt(
                          txt: userModel.name,
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 7),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          drawCircle(
                            context: context,
                            color: !isPublicUser
                                ? MyTheme.onlineColor
                                : (userModel.isOnline)
                                    ? MyTheme.onlineColor
                                    : MyTheme.offlineColor,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Txt(
                                txt: !isPublicUser
                                    ? 'task_alerts_edit_online'.tr
                                    : (userModel.isOnline)
                                        ? 'task_alerts_edit_online'.tr
                                        : 'profile_last_online_format'.tr +
                                            DateFun.getTimeAgoTxt(
                                                userModel.dateCreatedLocal),
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 7),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Image.asset(
                                "assets/images/icons/map_pin_icon.png",
                                width: 20,
                                height: 20,
                                color: Colors.white,
                              ),
                            ),
                            (address.length > 0)
                                ? Flexible(
                                    //flex: 3,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: Txt(
                                          txt: address ?? '',
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          maxLines: 2,
                                          isBold: false),
                                    ),
                                  )
                                : SizedBox(),
                          ]),
                    ),
                    SizedBox(height: 5),
                    Txt(
                        txt: (isLng(AppDefine.GEO_CODE))
                            ? (dt + "member_since".tr)
                            : ("member_since".tr + dt),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    isPublicUser
                        ? Column(
                            children: [
                              SizedBox(height: 30),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 40, right: 40),
                                child: MMBtn(
                                  txt: "profile_button_request_a_quote".tr,
                                  txtColor: Colors.white,
                                  bgColor: MyTheme.brandColor,
                                  height: getHP(context, MyTheme.btnHpa - .5),
                                  width: getW(context),
                                  callback: () {
                                    Get.to(
                                      () => AddTask1Screen(
                                        index: null,
                                        userModel: userModel,
                                      ),
                                    ).then((pageNo) {
                                      //obsUpdateTabs(pageNo);
                                    });
                                  },
                                ),
                              ),
                              SizedBox(height: 30),
                              GestureDetector(
                                onTap: () {
                                  Get.to(() => ResPage(
                                        from: ResCfg.RESOULUTION_TYPE_USER,
                                        userId: userModel.id,
                                      ));
                                },
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.flag_outlined,
                                        color: Colors.white,
                                      ),
                                      SizedBox(width: 10),
                                      Flexible(
                                        child: Txt(
                                            txt:
                                                "profile_report_user_button_label"
                                                    .tr,
                                            txtColor: Colors.white,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.center,
                                            isBold: false),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        : SizedBox(),
                    SizedBox(height: 20),
                  ],
                ),

                //ProfileHelper().getStarRatingView(rate: 4, reviews: 5),
                //SizedBox(height: 5),
                //ProfileHelper().getCompletionText(pa: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //  lISTVIEW  start here...

  drawUserSwitchView() {
    return Center(
      child: ToggleSwitch(
        isBorderColor: true,
        minWidth: getWP(context, 35),
        minHeight: getHP(context, MyTheme.switchBtnHpa),
        initialLabelIndex: posterSwitchValue,
        cornerRadius: 50.0,
        fontSize: isLng(AppDefine.GEO_CODE) ? 14 : 17,
        activeBgColor: MyTheme.brandColor,
        activeFgColor: Colors.white,
        inactiveBgColor: Colors.white,
        inactiveFgColor: MyTheme.brandColor,
        labels: ['as_tasker'.tr, 'as_poster'.tr],
        //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
        onToggle: (index) {
          posterSwitchValue = index;
          setState(() {});
        },
      ),
    );
  }

  drawUserRatingAndSummaryView(
      UserModel userModel,
      List<UserRatingSummaryDataModel> listUserRatingSummary,
      List<UserRatingsModel> listUserRating) {
    if (listUserRatingSummary == null) return SizedBox();

    int rate = 0;
    int completionRate = 0;
    int reviews = 0;
    //if (!isPublicUser) {
    if (listUserRatingSummary.length > 0) {
      if (posterSwitchValue == 1) {
        rate = listUserRatingSummary[0].posterAverageRating.toInt();
        completionRate = listUserRatingSummary[0].posterCompletionRate;
        reviews = listUserRatingSummary[0].posterRatingCount;
      } else {
        rate = listUserRatingSummary[0].taskerAverageRating.toInt();
        completionRate = listUserRatingSummary[0].taskerCompletionRate;
        reviews = listUserRatingSummary[0].taskerRatingCount;
      }
    }

    if (reviews == 0) {
      if (posterSwitchValue == 0) {
        final lookslikeStr = !isPublicUser
            ? "profile_no_reviews_runner".tr
            : (userModel.name + "profile_user_no_reviews_runner".tr);
        return Txt(
            txt: lookslikeStr,
            txtColor: MyTheme.gray5Color,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false);
      } else {
        var lookslikeStr = !isPublicUser
            ? "profile_no_reviews_created".tr
            : (userModel.name + "profile_user_no_reviews_created".tr);
        if (!isPublicUser)
          lookslikeStr = !isPublicUser
              ? "profile_no_reviews_runner".tr
              : (userModel.name + "profile_user_no_reviews_runner".tr);
        return Txt(
            txt: lookslikeStr,
            txtColor: MyTheme.gray5Color,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false);
      }
    } else {
      //} else {
      //rate = profileController.aveargeRatingAsTasker.value;
      //completionRate = profileController.completionRateAsTasker.value;
      //reviews = profileController.countAsTasker.value;
      //}

      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            UIHelper().getStarRatingView(rate: rate, reviews: reviews),
            //SizedBox(height: 5),
            UIHelper().getCompletionText(
              pa: completionRate,
              callbackInfo: () {
                if (posterSwitchValue == 0) {
                  showAlert(msg: "dialog_completion_rate_message_runner".tr);
                } else {
                  showAlert(msg: "dialog_completion_rate_message_sender".tr);
                }
              },
            ),
            ProfileHelper().drawProfileUserRatingView(listUserRating)
          ],
        ),
      );
    }
  }

  drawBadgesView(List<UserBadgesModel> listUserBadge) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "view_badges_badges_label".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            for (UserBadgesModel badgeModel in listUserBadge)
              drawBadgeList(badgeModel, isPublicUser, profileController),
          ],
        ),
      ),
    );
  }

  drawBadgeList(UserBadgesModel badgeModel, bool isPoster,
      ProfileController profileController) {
    try {
      if (!badgeModel.isVerified) return SizedBox();
      if (badgeModel.title == "") return SizedBox();

      var title = (badgeModel.title != null && !isPoster)
          ? badgeModel.title
          : TaskHelper().getBadgeTypeTxt(badgeModel.type);
      var activationDate = (badgeModel.verificationCode != null && !isPoster)
          ? DateFun.getTimeAgoTxt(badgeModel.creationDate)
          : "";
      //var refCode = "";
      bool isBadgeStatus101 = (badgeModel.status == 101) ? true : false;
      profileController.setBadgeValue(isBadgeStatus101.obs);
      return (isBadgeStatus101)
          ? Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 30,
                          height: 30,
                          child: TaskHelper().getBadgeTypeSvg(badgeModel.type),
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: title,
                                    txtColor: MyTheme.gray5Color,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                (activationDate != '')
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Txt(
                                            txt: activationDate,
                                            txtColor: MyTheme.gray4Color,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                      )
                                    : SizedBox()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          : SizedBox();
    } catch (e) {
      return SizedBox();
    }
  }

  drawPortFolioView(UserPortfulioModel userPortfulioModel) {
    if (userPortfulioModel == null) return SizedBox();
    final listUrl = userPortfulioModel.portfulioItems.split("|");
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "profile_screen_portfolio_section_label".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 20),
            Container(
              height: getHP(context, 30),
              child: ListView.separated(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.horizontal,
                itemCount: listUrl.length,
                separatorBuilder: (BuildContext context, int index) =>
                    Container(width: 10),
                itemBuilder: (context, index) {
                  return Center(
                    child: new Container(
                      width: getWP(context, listUrl.length == 1 ? 90 : 85),
                      child: GestureDetector(
                        onTap: () {
                          Get.to(() => PicFullView(
                                title: "full_screen_image_screen_title".tr,
                                url: listUrl[index],
                              ));
                        },
                        child: MyNetworkImage()
                            .loadCacheImage(url: listUrl[index]),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawMoreButtonView() {
    return Obx(
      () => Container(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
          child: Column(
            children: [
              (!profileController.isBadgeStatus101.value)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Txt(
                          txt: "view_user_no_badges_yet".tr,
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    )
                  : SizedBox(),
              SizedBox(height: 20),
              Container(
                width: getWP(context, 100),
                height: getHP(context, MyTheme.btnHpa),
                child: BtnOutline(
                    width: null,
                    height: null,
                    txt: "task_details_missing_badge_learn_more".tr,
                    radius: 30,
                    txtColor: MyTheme.brandColor,
                    borderColor: MyTheme.gray3Color,
                    callback: () {
                      Get.to(() => WebScreen(
                            //'http://192.168.1.100/mm/'
                            url: ServerUrls.BADGE_SUPPORT_URL,
                            title: 'Learn More',
                          ));
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  drawAboutSkillsView(AboutModel aboutModel) {
    if (aboutModel == null) return SizedBox();
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "skills_label".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 20),
            _drawSkills("transportation_label".tr, aboutModel.goAround ?? ''),
            _drawSkills("languages_label".tr, aboutModel.languages ?? ''),
            _drawSkills("education_label".tr, aboutModel.qualifications ?? ''),
            _drawSkills("work_label".tr, aboutModel.experiences ?? ''),
            _drawSkills(
                "specialities_label".tr, aboutModel.whatIamlookingfor ?? ''),
            SizedBox(height: 20),
            Txt(
                txt: aboutModel.remarks,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }

  _drawSkills(String title, String str) {
    final _controller = ScrollController();
    if (str == '') return SizedBox();
    final arr = str.split("|").reversed.toList();
    //
    bool isShowLRArrow = false;
    int h = 0;
    int txtLen = 0;
    for (var t in arr) txtLen += t.length;
    final txtFontSize = txtLen * 12;
    h += txtFontSize;
    if (h > getWP(context, 90)) {
      isShowLRArrow = true;
    }
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Txt(
                      txt: title,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                (isShowLRArrow)
                    ? Row(
                        children: [
                          IconButton(
                            icon:
                                Icon(Icons.arrow_back_ios, color: Colors.grey),
                            onPressed: () {
                              var pos = _controller.position.pixels - 200;
                              if (pos < 0) pos = 0;
                              _controller.animateTo(pos,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.fastOutSlowIn);
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios,
                                color: Colors.grey),
                            onPressed: () {
                              final pos = _controller.position.pixels + 200;
                              _controller.animateTo(pos,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.fastOutSlowIn);
                            },
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Container(
                width: getW(context),
                height: 30,
                child: ListView.builder(
                  controller: _controller,
                  primary: false,
                  shrinkWrap: true,
                  itemCount: arr.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          color: MyTheme.gray2Color,
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: Colors.grey, width: .5)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Center(
                          child: Txt(
                              txt: arr[index],
                              txtColor: MyTheme.blueColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            //(arr.length == 0) ? SizedBox(height: 10) : SizedBox(),
            Container(
              color: Colors.grey,
              height: .5,
            ),
          ],
        ),
      ),
    );
  }
}
