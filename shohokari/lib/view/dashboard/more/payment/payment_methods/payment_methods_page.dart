import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/make_payment/make_payment_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/receive_payment/receive_payment_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'make_payment/make_payment_base.dart';

class PaymentMethodsPage extends StatefulWidget {
  const PaymentMethodsPage({Key key}) : super(key: key);
  @override
  State createState() => _PaymentMethodsPageState();
}

class _PaymentMethodsPageState extends State<PaymentMethodsPage>
    with Mixin, StateListener {
  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        final audioController = Get.put(AudioController());
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("more_payment_method");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: MyTheme.appbarElevation,
            backgroundColor: MyTheme.bgColor,
            iconTheme: MyTheme.themeData.iconTheme,
            title: UIHelper().drawAppbarTitle(title: 'payment_settings'.tr),
            centerTitle: false,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 7)),
              child: Container(
                //color: MyTheme.redColor,
                height: getHP(context, MyTheme.btnHpa),
                child: TabBar(
                  //labelColor: Colors.deepOrange,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 3,
                  tabs: [
                    Container(
                        //color: MyTheme.blueColor,
                        //height: getHP(context, MyTheme.btnHpa),
                        child: Txt(
                      txt: "payment_settings_tab_title_make_payments"
                          .tr
                          .toUpperCase(),
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                    Container(
                        child: Txt(
                      txt: "payment_settings_tab_title_receive_payments"
                          .tr
                          .toUpperCase(),
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                  ],
                ),
              ),
            ),
          ),
          body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: TabBarView(
                children: [
                  MakePaymentPage(),
                  ReceivePaymentPage(),
                ],
              )),
        ),
      ),
    );
  }
}
