import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/more/db/db_page.dart';
import 'package:aitl/view/dashboard/more/help/help_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_history_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/payment_methods_page.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/dashboard/more/reviews/reviews_page.dart';
import 'package:aitl/view/dashboard/more/settings/settings_page.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_page.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../dashboard_page.dart';
import 'help/help_page.dart';
import 'more_base.dart';

enum eMoreEvent {
  app_tut1,
  signout,
}

class MorePage extends StatefulWidget {
  @override
  State createState() => MorePageState();
}

class MorePageState extends BaseMoreStatefull<MorePage> with StateListener {
  //  **************  app states start
  var unreadNotiCount = 0;

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        final myTaskController = Get.put(MyTaskController());
        myTaskController.setTaskModel(null);
        unreadNotiCount =
            await PrefMgr.shared.getPrefInt("unreadNotificationCount");
        StateProvider().notify(ObserverState.STATE_BOTNAV, null);
        setState(() {});
      } else if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        final myTaskController = Get.put(MyTaskController());
        myTaskController.setTaskModel(null);
        //
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("browse_task");
        //
        unreadNotiCount =
            await PrefMgr.shared.getPrefInt("unreadNotificationCount");
        StateProvider().notify(ObserverState.STATE_BOTNAV, null);
        setState(() {});
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  //  **************  app states end

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  appInit() async {
    try {
      try {
        _stateProvider = new StateProvider();
        _stateProvider.subscribe(this);
      } catch (e) {}
      unreadNotiCount =
          await PrefMgr.shared.getPrefInt("unreadNotificationCount");
    } catch (e) {}
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: UIHelper().drawAppbarTitle(title: 'more'.tr),
          centerTitle: false,
          actions: drawSoundLngBox(this.runtimeType, audioController, []),
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    listMore = [
      {"title": "dashboard".tr, "route": () => DashboardMore()},
      {"title": "profile".tr, "route": () => ProfilePage()},
      {"title": "payment_history".tr, "route": () => PaymentHistoryPage()},
      {"title": "payment_methods".tr, "route": () => PaymentMethodsPage()},
      {"title": "reviews".tr, "route": () => ReviewsPage()},
      {"title": "notifications".tr, "route": () => NotiPage()},
      {"title": "task_alerts_setting".tr, "route": () => TaskAlertPage()},
      {"title": "settings".tr, "route": () => SettingsPage()},
      {"title": "help".tr, "route": () => HelpPage()},
      {"title": "logout".tr, "route": null},
    ];

    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: ListView.builder(
          itemCount: listMore.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> mapMore = listMore[index];
            return GestureDetector(
              onTap: () async {
                try {
                  final myTaskController = Get.put(MyTaskController());
                  myTaskController.setTaskModel(null);
                  myTaskController.dispose();
                } catch (e) {}
                if (mapMore['route'] != null) {
                  Get.to(mapMore['route']).then((value) {
                    if (value != null) {
                      if (value['moreEvent'] == eMoreEvent.app_tut1) {
                        DashboardPageState.isDialogHelpOpenned = false;
                        StateProvider()
                            .notify(ObserverState.STATE_OPEN_HELP_DIALOG, null);
                      }
                    } else {
                      if (mapMore['title'] == 'notifications'.tr) {
                        unreadNotiCount =
                            userData.userModel.unreadNotificationCount;
                        StateProvider()
                            .notify(ObserverState.STATE_BOTNAV, null);
                        setState(() {});
                      }
                    }
                  });
                } else {
                  showConfirmDialog(
                    context: context,
                    title: "log_out".tr,
                    msg: "confirm_logout".tr,
                    callback: () {
                      StateProvider().notify(ObserverState.STATE_LOGOUT, null);
                    },
                  );
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Txt(
                                txt: mapMore['title'].toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          (mapMore['title'] == 'notifications'.tr &&
                                  userData.userModel.unreadNotificationCount >
                                      0 &&
                                  userData.userModel.unreadNotificationCount !=
                                      unreadNotiCount)
                              ? drawNotiBadge()
                              : SizedBox(),
                          (index != listMore.length - 1)
                              ? Icon(
                                  Icons.arrow_forward_ios,
                                  color: MyTheme.gray3Color,
                                  size: 20,
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    drawLine(),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
