import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/ratings/TaskUserRatingAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class ReviewPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const ReviewPage({Key key, this.taskBidding}) : super(key: key);
  @override
  State createState() => _ReviewPageState();
}

class _ReviewPageState extends BasePaymentStatefull<ReviewPage>
    with APIStateListener {
  final cmt = TextEditingController();
  int rating = 0;

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.post_review &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final userRatingId =
                (model as TaskUserRatingAPIModel).responseData.userRating.id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TASK_USER_RATING_URL
                  .replaceAll("#userRatingId#", userRatingId.toString()),
              isLoading: true,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: true);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  wsPostReview() async {
    try {
      int employeeId = 0;
      bool isPoster = false;
      if (widget.taskBidding.isTaskOwner) {
        employeeId = widget.taskBidding.userId;
        isPoster = true;
      } else {
        employeeId = myTaskController.getTaskModel().userId;
        isPoster = false;
      }

      await APIViewModel().req<TaskUserRatingAPIModel>(
        context: context,
        apiState: APIState(APIType.post_review, this.runtimeType, null),
        url: APIProfileCFg.USER_RATING_POST_URL,
        isLoading: true,
        reqType: ReqType.Post,
        param: {
          "Comments": cmt.text.trim(),
          "CreationDate": DateTime.now().toString(),
          "EmployeeId": employeeId,
          "Rating": rating,
          "Status": myTaskController.getStatusCode(),
          "TaskBiddingId": widget.taskBidding.id,
          "TaskTitle": myTaskController.getTaskModel().title,
          "UserId": userData.userModel.id,
          "Id": 0, //myTaskController.getTaskModel().id,
        },
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    cmt.dispose();
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: "leave_review".tr),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawAvatorView(),
            SizedBox(height: 20),
            drawStarRatingView(),
            drawReviewBox(),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MMBtn(
                txt: "leave_review_button_label".tr,
                height: getHP(context, MyTheme.btnHpa),
                width: getW(context),
                callback: () async {
                  wsPostReview();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawAvatorView() {
    return Container(
      color: MyTheme.gray1Color,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircleAvatar(
              radius: 30,
              backgroundColor: Colors.transparent,
              backgroundImage: MyNetworkImage.loadProfileImage(
                  myTaskController.getTaskModel().ownerImageUrl),
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: myTaskController.getTaskModel().ownerName,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize + .3,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 5),
                  Txt(
                      txt: myTaskController.getTaskModel().title,
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ],
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Txt(
                      txt: "task_price2".tr,
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 5),
                  Txt(
                      txt: getCurSign() +
                          widget.taskBidding.netTotalAmount.round().toString(),
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize + .5,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawStarRatingView() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                text: TextSpan(
                  text: 'leave_review_leave_review_for'.tr,
                  style: TextStyle(color: MyTheme.gray4Color, fontSize: 20),
                  children: <TextSpan>[
                    TextSpan(
                        text: myTaskController.getTaskModel().ownerName,
                        style:
                            TextStyle(color: MyTheme.blueColor, fontSize: 20)),
                  ],
                ),
              ),
              SizedBox(width: 10),
              CircleAvatar(
                radius: 30,
                backgroundColor: Colors.transparent,
                backgroundImage: MyNetworkImage.loadProfileImage(
                    myTaskController.getTaskModel().ownerImageUrl),
              ),
            ],
          ),
          SizedBox(height: 10),
          RatingBar.builder(
            initialRating: 0,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: false,
            itemCount: 5,
            itemPadding: EdgeInsets.symmetric(horizontal: 2),
            itemSize: 50,
            unratedColor: Colors.grey.withAlpha(100),
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: MyTheme.brandColor,
            ),
            onRatingUpdate: (rating2) {
              rating = rating2.round();
            },
          ),
          SizedBox(height: 10),
          Txt(
              txt: "leave_review_rating_bar_label".tr,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.center,
              isBold: false),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  drawReviewBox() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Container(
          //padding: const EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: TextFormField(
              controller: cmt,
              minLines: 5,
              maxLines: 10,
              autocorrect: false,
              onChanged: (v) {},
              keyboardType: TextInputType.multiline,
              style: TextStyle(
                color: Colors.black,
                fontSize:
                    getTxtSize(context: context, txtSize: MyTheme.txtSize),
              ),
              decoration: new InputDecoration(
                counter: Offstage(),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintText: "leave_review_review_hint".tr,
                hintStyle: new TextStyle(
                  color: Colors.grey,
                  fontSize:
                      getTxtSize(context: context, txtSize: MyTheme.txtSize),
                ),
                contentPadding: const EdgeInsets.symmetric(vertical: 0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
