import 'dart:io';
import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppShareCfg.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:share/share.dart';
import 'taskdetails_base.dart';

class TaskDetailsPage extends StatefulWidget {
  final TaskModel taskModel;
  final String description, title, body;
  const TaskDetailsPage({
    Key key,
    @required this.taskModel,
    this.description,
    this.title,
    this.body,
  }) : super(key: key);
  @override
  State createState() => _TaskDetailsPageState();
}

class _TaskDetailsPageState extends BaseTaskDetailsStatefull<TaskDetailsPage>
    with APIStateListener, StateListener {
  List<TaskBiddingModel> listTaskBiddings;
  List<GetPicModel> listGetPicModel;
  List<TimelinePostModel> listTimelinePostsModel = [];
  List<UserRatingsModel> listUserRatings = [];

  final TextEditingController textController = TextEditingController();
  final ScrollController scrollController = new ScrollController();
  String comments = '';

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TASKDETAILS_GET_TIMELINE) {
        wsGetTimelineByApp();
        setState(() {});
      }
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("task_details_offer");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_timeline_by_app &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listTimelinePostsModel =
                (model as GetTimeLineByAppAPIModel).responseData.timelinePosts;
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.task_biddings &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final listTaskBidding2 =
                (model as TaskBiddingsAPIModel).responseData.taskBiddings;
            listTaskBiddings = [];
            for (var model2 in listTaskBidding2) {
              //if (isTaskBiddingStatusOK(myTaskController, model2)) {
              //if (model2.userId == userData.userModel.id || !isPoster) {
              listTaskBiddings.add(model2);
              //}
              //}
            }
            try {
              if (myTaskController.getTaskModel().workerNumber > 1 &&
                  listTaskBiddings[0].status ==
                      TaskStatusCfg()
                          .getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED)) {
                isPaymentDue = true;
              }
            } catch (e) {}
            //log(listTaskBidding);
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.user_rating &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRatings =
                (model as UserRatingAPIModel).responseData.userRatings;
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.get_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listGetPicModel =
                (model as GetPicAPIModel).responseData.taskPictures;
            listGetPicModel =
                (listGetPicModel.length == 0) ? null : listGetPicModel;
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostTimeline((model as MediaUploadFilesAPIModel));
          }
        }
      } else if (apiState.type == APIType.post_timeline &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetTimelineByApp();
            setState(() {
              Future.delayed(const Duration(seconds: 1), () {
                scrollDown(scrollController,
                    getH(context) * AppConfig.chatScrollHeight);
              });
            });
            final timelineId =
                (model as TimelinePostAPIModel).responseData.post.id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TIMELINE_EMAI_NOTI_GET_URL
                  .replaceAll("#timelineId#", timelineId.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      } else if (apiState.type == APIType.del_task &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              myTaskController.setTaskModel(null);
              Get.back(result: true);
            } catch (e) {}
          }
        }
      } else if (apiState.type == APIType.task_bidding_del &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: true);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  onDelTaskClicked() {
    try {
      APIViewModel().req<DelTaskAPIModel>(
        context: context,
        apiState: APIState(APIType.del_task, this.runtimeType, null),
        url: APIPostTaskCfg.DEL_TASK_URL.replaceAll(
            "#taskId#", myTaskController.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  wsWithdrawTaskbidding() async {
    try {
      await APIViewModel().req<CommonAPIModel>(
        context: context,
        apiState: APIState(APIType.task_bidding_del, this.runtimeType, null),
        url: APIMyTasksCfg.DEL_TASKBIDDING_URL
            .replaceAll("#taskBiddingId#", listTaskBiddings[0].id.toString()),
        isLoading: true,
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  wsPostTimeline(MediaUploadFilesAPIModel model) async {
    try {
      //final jsonRes = json.encode(model.jsonRes);
      var res = '';
      try {
        res = model.jsonRes.toString();
      } catch (e) {}
      await APIViewModel().req<TimelinePostAPIModel>(
        context: context,
        apiState: APIState(APIType.post_timeline, this.runtimeType, null),
        url: APITimelineCfg.TIMELINE_POST_URL,
        isLoading: false,
        reqType: ReqType.Post,
        param: {
          "AdditionalAttributeValue": res ?? '',
          "Checkin": "",
          "FromLat": 0.0,
          "FromLng": 0.0,
          "IsPrivate": false,
          "Message": comments,
          "OwnerId": userData.userModel.id,
          "PostTypeName": "status", //(url == null) ? "status" : "picture",
          "TaskId": myTaskController.getTaskModel().id,
        },
      );
    } catch (e) {}
  }

  wsGetTimelineByApp() async {
    try {
      await APIViewModel().req<GetTimeLineByAppAPIModel>(
        context: context,
        apiState: APIState(APIType.get_timeline_by_app, this.runtimeType, null),
        url: APITimelineCfg.GET_TIMELINE_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": 8,
          "CustomerId": 0,
          "IsPrivate": false,
          "Page": 1,
          "TaskId": myTaskController.getTaskModel().id,
          "timeLineId": 0,
        },
      );
    } catch (e) {}
  }

  Future<void> getRefreshData() async {
    try {
      await wsGetTimelineByApp();
      await APIViewModel().req<TaskBiddingsAPIModel>(
        context: context,
        apiState: APIState(APIType.task_biddings, this.runtimeType, null),
        url: APIMyTasksCfg.GET_TASKBIDDING_URL
            .replaceAll("/#taskBiddingId#", ""),
        reqType: ReqType.Get,
        param: {
          "Count": 50,
          "IsAll": true,
          "Page": 0,
          "TaskId": myTaskController.getTaskModel().id,
          "userId": userData.userModel.id,
        },
      );
      await APIViewModel().req<GetPicAPIModel>(
        context: context,
        apiState: APIState(APIType.get_pic, this.runtimeType, null),
        reqType: ReqType.Get,
        url: APIPostTaskCfg.GET_PIC_URL.replaceAll(
          "#taskId#",
          myTaskController.getTaskModel().id.toString(),
        ),
      );

      await APIViewModel().req<UserRatingAPIModel>(
        context: context,
        apiState: APIState(APIType.user_rating, this.runtimeType, null),
        url: APIMyTasksCfg.GET_USERRATING_BYTASKID_URL.replaceAll(
            "#taskId#", myTaskController.getTaskModel().id.toString()),
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    indexTopBtn.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    comments = null;
    listTaskBiddings = null;
    listGetPicModel = null;
    listUserRatings = null;
    listTimelinePostsModel = null;
    textController.dispose();
    scrollController.dispose();
    try {
      myTaskController.dispose();
      taskDetController.dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}

    indexTopBtn = ValueNotifier<int>(0);

    try {
      myTaskController.setTaskModel(widget.taskModel);
    } catch (e) {}

    if (myTaskController.getTaskModel().userId == userData.userModel.id) {
      isPoster = false;
    }

    log(myTaskController.getTaskModel().id);

    getRefreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'task_details'.tr),
          centerTitle: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () async {
                final msg = AppShareCfg.SERVER_SHARE_PUBLIC_URL +
                    userData.userModel.userProfileUrl;
                await Share.share(msg);
              },
            ),
            getPopupMenuButton(),
          ],
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: RefreshIndicator(
        color: Colors.white,
        backgroundColor: MyTheme.brandColor,
        onRefresh: getRefreshData,
        child: ListView(
          controller: scrollController,
          shrinkWrap: true,
          //primary: true,
          children: [
            SizedBox(height: 20),
            drawTopStatus(),
            drawTitle(),
            drawPostedBy(),
            drawLocation(),
            drawDueDate(),
            drawPriceBox(listTaskBiddings),
            drawProjectSummary(),
            drawDetailsView(listGetPicModel),
            drawReviews(listUserRatings),
            drawPrivateMsg(),
            drawOffers(listTaskBiddings, false),
            drawQuestions(
                listTimelinePostsModel: listTimelinePostsModel,
                textController: textController,
                cls: this.runtimeType,
                callbackCam: (File file, String txt) async {
                  comments = txt.trim();
                  textController.clear();
                  if (file != null) {
                    await APIViewModel().upload(
                      context: context,
                      apiState: APIState(
                          APIType.media_upload_file, this.runtimeType, null),
                      file: file,
                    );
                  } else {
                    wsPostTimeline(null);
                  }
                }),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
