import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import '../taskdetails_base.dart';

class ConfirmOfferPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const ConfirmOfferPage({Key key, @required this.taskBidding})
      : super(key: key);
  @override
  State createState() => _ConfirmOfferPageState();
}

class _ConfirmOfferPageState
    extends BaseTaskDetailsStatefull<ConfirmOfferPage> {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  getRefreshData() {}
  wsGetTimelineByApp() {}
  wsWithdrawTaskbidding() {}
  onDelTaskClicked() {}

  appInit() async {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'update_make_offer'.tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context, text: "continue".tr, callback: () async {}),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          (widget.taskBidding == null) ? drawNF() : makeOffers(),
        ],
      ),
    );
  }

  makeOffers() {
    return Card(
      color: MyTheme.gray1Color,
      child: Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: Column(
          children: [
            Txt(
                txt: "make_offer_your_offer_label".tr,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }

  drawNF() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  "assets/images/icons/ic_waiting_offers.png",
                  fit: BoxFit.cover,
                  width: getW(context) / 3.5,
                  height: getW(context) / 5,
                ),
              ),
              SizedBox(height: 10),
              Txt(
                  txt: "no_offers_yet_message".tr,
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
