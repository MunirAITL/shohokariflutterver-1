import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/data/translations/LanguageTranslations.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:auto_size_text/auto_size_text.dart';

class Txt extends StatelessWidget {
  final controllerLng = Get.put(LngTRController());
  final txt;
  Color txtColor;
  double txtSize;
  double txtLineSpace;
  TextAlign txtAlign;
  final TextStyle style;
  final isBold;
  final isOverflow;
  final int maxLines;

  Txt({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.txtSize,
    @required this.txtAlign,
    @required this.isBold,
    this.txtLineSpace,
    this.isOverflow = false,
    this.maxLines,
    this.style,
    CrossAxisAlignment crossAxisAlignment,
    MainAxisAlignment mainAxisAlignment,
    List children,
  }) {
    if (this.txtLineSpace == null) {
      this.txtLineSpace = MyTheme.txtLineSpace;
    }
  }

  @override
  Widget build(BuildContext context) {
    return //FittedBox(
        //fit: BoxFit.fitWidth,
        //child:
        Text(
      Common.parseHtmlString(txt),
      textAlign: txtAlign,
      maxLines: maxLines,
      overflow: (isOverflow) ? TextOverflow.ellipsis : TextOverflow.visible,
      style: (style != null)
          ? style
          : TextStyle(
              height: txtLineSpace,
              fontSize: ResponsiveFlutter.of(context).fontSize(
                  (controllerLng.local.value.countryCode == AppDefine.GEO_CODE)
                      ? txtSize - .2
                      : txtSize),
              color: txtColor,
              fontWeight: (isBold) ? FontWeight.bold : FontWeight.normal,
            ),
      //),
    );
  }
}

extension CustomStyles on TextTheme {
  TextStyle get error {
    return TextStyle(
      fontSize: 20.0,
      color: Colors.black,
      fontWeight: FontWeight.bold,
    );
  }
}
