import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/auth_base.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/widgets/btn/BtnOutlineIcon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../txt/Txt.dart';

class SMS1Dialog extends StatefulWidget {
  @override
  State createState() => SMS1DialogState();
}

class SMS1DialogState extends BaseAuth<SMS1Dialog> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      child: Padding(
        padding:
            const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              /*Align(
                alignment: Alignment.topRight,
                child: IconButton(
                    iconSize: 20,
                    icon: Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Get.back();
                    }),
              ),*/
              SizedBox(height: 10),
              BtnOutlineIcon(
                txt: "sign_up_login_main_button_label_mobile_login".tr,
                txtColor: MyTheme.brandColor,
                borderColor: Colors.grey,
                bgColor: Colors.white,
                leftIcon: "assets/images/svg/ico_phone.svg",
                leftIconColor: MyTheme.brandColor,
                radius: 50,
                callback: () {
                  Get.to(
                    () => Sms2Page(),
                  ).then((value) {
                    Get.off(() => AuthScreen());
                  });
                },
              ),
              SizedBox(height: 10),
              Txt(
                txt: "sign_up_login_separator_or".tr,
                txtColor: Colors.black54,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 10),
              BtnOutlineIcon(
                txt: "fb_login".tr,
                txtColor: Colors.white,
                borderColor: Colors.transparent,
                bgColor: MyTheme.fbColor,
                leftIcon: "assets/images/svg/ic_fb.svg",
                leftIconColor: Colors.white,
                radius: 50,
                callback: () {
                  loginWithGoogle(this.runtimeType);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
