import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class AlertDialogCustom extends StatelessWidget with Mixin {
  Function onConfirmClick;
  Function onCancelClick;

  AlertDialogCustom({this.onConfirmClick, this.onCancelClick});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        height: MediaQuery.of(context).size.height / 1.5,
        width: MediaQuery.of(context).size.width / 1.1,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Txt(
                    txt: "Are you sure want to submit your application?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.appbarTitleFontSize,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Txt(
                    txt:
                        """Once the application is submitted our advisers will begin to process the application. The information you have provided will be checked for verification. We may also conduct credit searches and other due-diligence as a part of processing your application and you are hereby giving us explicit permission to do so.""",
                    txtColor: Colors.black38,
                    txtAlign: TextAlign.justify,
                    txtSize: 1.5,
                    isBold: true),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Txt(
                    txt:
                        """By submitting this application you are confirming that you have read the Terms and Conditions and you declare that the information provided in this fact-find is true and accurate.""",
                    txtColor: Colors.black38,
                    txtAlign: TextAlign.justify,
                    txtSize: 1.5,
                    isBold: true),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Txt(
                    txt:
                        "Once submitted you can no longer edit this application.",
                    txtColor: Colors.red,
                    txtSize: 2,
                    txtAlign: TextAlign.left,
                    isBold: false),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Btn(
                        txt: "Cancel",
                        txtColor: Colors.black,
                        bgColor: Colors.grey[300],
                        callback: onCancelClick),
                    Btn(
                        txt: "Confirm",
                        txtColor: Colors.white,
                        bgColor: Colors.blue,
                        callback: onConfirmClick),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
