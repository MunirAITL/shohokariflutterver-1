import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../mixin.dart';

confirmDialog(
    {BuildContext context,
    String title = "",
    String msg = "",
    int which = 0,
    Function callback}) {
  return Dialog(
    backgroundColor: Colors.transparent,
    child: Card(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Icon(
              (which == 2)
                  ? Icons.info
                  : (which == 1)
                      ? Icons.check
                      : Icons.error_rounded,
              size: 50,
              color: (which == 2)
                  ? Colors.orange
                  : (which == 1)
                      ? Colors.green
                      : Colors.red,
            ),
            (title != null)
                ? Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Txt(
                      txt: title,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize + .2,
                      txtAlign: TextAlign.center,
                      isBold: true,
                      //txtLineSpace: 1.5,
                    ),
                  )
                : SizedBox(),
            Txt(
              txt: msg,
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false,
              //txtLineSpace: 1.5,
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: TextButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.grey.shade400)),
                    onPressed: () {
                      Get.back();
                    },
                    child: Text(
                      "no".tr,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Container(
                    alignment: Alignment.center,
                    child: TextButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              HexColor.fromHex("#00ff00"))),
                      onPressed: () {
                        Get.back();
                        callback();
                      },
                      child: Text(
                        "yes".tr,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

/*class ConfirmDialog extends StatelessWidget {
  final title, msg;
  final Function callback;
  const ConfirmDialog({
    Key key,
    @required this.callback,
    @required this.title,
    @required this.msg,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              (title != null)
                  ? Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Txt(
                        txt: title,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .2,
                        txtAlign: TextAlign.center,
                        isBold: false,
                        //txtLineSpace: 1.5,
                      ),
                    )
                  : SizedBox(),
              Txt(
                txt: msg,
                txtColor: Colors.black87,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Btn(
                    txt: "no".tr,
                    txtColor: Colors.black,
                    bgColor: MyTheme.greyColor,
                    callback: () {
                      Get.back();
                    },
                  ),
                  Btn(
                    txt: "yes".tr,
                    txtColor: Colors.black,
                    bgColor: MyTheme.greyColor,
                    callback: () {
                      callback();
                      Get.back();
                    },
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}*/
