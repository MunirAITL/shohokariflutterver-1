import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

showDeactivateProfileDialog(
    {BuildContext context,
    TextEditingController email,
    Function callback}) async {
  await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            backgroundColor: Colors.white,
            title: Row(
              children: [
                Expanded(
                  child: Txt(
                      txt: "myprofile".tr,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                IconButton(
                    iconSize: 20,
                    icon: Icon(Icons.close, color: Colors.black),
                    onPressed: () {
                      Navigator.pop(context);
                      //callback(null);
                    })
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextField(
                    controller: email,
                    keyboardType: TextInputType.emailAddress,
                    minLines: 2,
                    maxLines: 3,
                    maxLength: 255,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize),
                    ),
                    decoration: InputDecoration(
                      counter: Offstage(),
                      hintText: 'reason'.tr + '...',
                      hintStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: ResponsiveFlutter.of(context)
                            .fontSize(MyTheme.txtSize),
                      ),
                      hintMaxLines: 1,
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1),
                      ),
                      border: new OutlineInputBorder(),
                    ),
                  ),
                  Center(
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                        callback(email.text.trim());
                      },
                      child: Txt(
                          txt: "deactivate".tr,
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false),
                      color: MyTheme.redColor,
                    ),
                  ),
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)));
      });
}
