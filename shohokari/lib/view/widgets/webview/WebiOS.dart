import 'package:aitl/view/widgets/webview/WebBase.dart';
import 'package:aitl/view_model/generic/enum_gen.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class WebiOS extends StatefulWidget {
  final String url;
  const WebiOS({Key key, @required this.url}) : super(key: key);
  @override
  _WebiOSState createState() => _WebiOSState();
}

class _WebiOSState extends WebBase<WebiOS> with StateListener {
  InAppWebViewController webViewCtrl;
  InAppWebView inAppWebView;

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state, data) async {
    if (state == ObserverState.STATE_WEBVIEW_BACK) {
      webViewCtrl.goBack();
    }
  }

  onHideWebview() {
    //inAppWebView
  }
  onShowWebview() {}

  @override
  void initState() {
    super.initState();
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);
    inAppWebView = InAppWebView(
      onLoadError: (controller, url, code, message) {
        //showToast(context: context, msg: "Please try again.");
        print("load error === " + message);
      },
      //initialHeaders: {'Cookie': cookieStr},
      initialUrlRequest: URLRequest(url: Uri.parse(widget.url)),
      initialOptions: InAppWebViewGroupOptions(
        crossPlatform: InAppWebViewOptions(
          //minimumFontSize: 14,
          // debuggingEnabled: true,
          javaScriptEnabled: true,
          javaScriptCanOpenWindowsAutomatically: true,
          useShouldOverrideUrlLoading: true,
          mediaPlaybackRequiresUserGesture: false,
          useShouldInterceptAjaxRequest: true,
          useShouldInterceptFetchRequest: true,
          //useOnDownloadStart: true,
          //useOnDownloadStart: true,
          //useOnLoadResource: true,
          verticalScrollBarEnabled: true,
          horizontalScrollBarEnabled: true,
          preferredContentMode: UserPreferredContentMode.MOBILE,
        ),
        ios: IOSInAppWebViewOptions(
          allowsInlineMediaPlayback: true,
        ),
        android: AndroidInAppWebViewOptions(
            //minimumLogicalFontSize: 1,
            //saveFormData: true,
            //hardwareAcceleration: true,
            //domStorageEnabled: true,
            //databaseEnabled: true,
            //clearSessionCache: true,
            //thirdPartyCookiesEnabled: true,
            //allowUniversalAccessFromFileURLs: true,
            //allowFileAccess: true,
            //allowContentAccess: true,
            //useWideViewPort: false,
            ),
      ),
      androidOnPermissionRequest: (controller, origin, resources) async {
        return PermissionRequestResponse(
            resources: resources,
            action: PermissionRequestResponseAction.GRANT);
      },
      onProgressChanged: (InAppWebViewController controller, int progress2) {
        webviewController.progress.value = progress2;
        print(webviewController.progress.value.toStringAsFixed(1));
      },
      onLoadStart: (controller, url) {
        webviewController.isLoading.value = true;
      },
      onLoadStop: (controller, url) {
        webviewController.isLoading.value = false;
      },
      onWebViewCreated: (controller) {
        webViewCtrl = controller;
        controller.addJavaScriptHandler(
            handlerName: EnumGen.getEnum2Str(eHandlers.Back),
            callback: (message) {
              onBack();
            });

        controller.addJavaScriptHandler(
            handlerName: EnumGen.getEnum2Str(eHandlers.DownloadFileModule),
            callback: (message) async {
              if (message == null) return;
              if (message.length == 0) return;
              if (message[0] != "") {
                print(message[0]);
                onDownloadFileModule(message[0]);
              }
            });

        controller.addJavaScriptHandler(
            handlerName: EnumGen.getEnum2Str(eHandlers.FullViewFileHandler),
            callback: (message) async {
              if (message == null) return;
              if (message.length == 0) return;
              if (message[0] != "") {
                print(message[0]);
                onFullViewFileHandler(message[0], (url) {
                  controller.loadUrl(
                      urlRequest: URLRequest(url: Uri.parse(url)));
                });
                //onSubmitApplication(widget, "SubmitApplication");
              }
            });
      },
    );
  }

  @override
  void dispose() {
    inAppWebView = null;
    webViewCtrl = null;
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return inAppWebView;
  }
}
