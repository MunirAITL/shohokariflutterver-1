import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/webview/WebAndroid.dart';
import 'package:aitl/view/widgets/webview/WebiOS.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'WebBase.dart';

class WebScreen extends StatefulWidget {
  final String url;
  final String title;
  final String caseID;

  const WebScreen({
    Key key,
    this.caseID,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> with Mixin {
  final webviewController = Get.put(WebviewController());
  final CookieManager cookieManager = CookieManager.instance();

  var cookieStr;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    webviewController.dispose();
    cookieStr = null;
    //flutterWebviewPlugin.close();
    //flutterWebviewPlugin.dispose();
    //webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      CookieJar cj = await CookieMgr().getCookiee();
      var listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      log(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );
      }
      setState(() {});
    } catch (e) {
      cookieStr = "";
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        //resizeToAvoidBottomInset: true,
        //resizeToAvoidBottomPadding :true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(
                webviewController.appTitle.value == ""
                    ? Icons.arrow_back
                    : Icons.close,
                color: Colors.white),
            onPressed: () async {
              if (webviewController.appTitle.value == "")
                Navigator.pop(context);
              else {
                //flutterWebviewPlugin.goBack();
                StateProvider().notify(ObserverState.STATE_WEBVIEW_BACK, null);
                webviewController.appTitle.value = "";
              }
            },
          ),
          title: (webviewController.progress.value < 100)
              ? Container(
                  width: getW(context),
                  child: Text(webviewController.progress.value.toString() + "%",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      )),
                )
              : FittedBox(
                  fit: BoxFit.fitWidth,
                  child: AutoSizeText(
                      (webviewController.appTitle.value == "")
                          ? widget.title
                          : webviewController.appTitle.value,
                      style: TextStyle(
                          color: Colors.white,
                          //fontSize: 17,
                          fontWeight: FontWeight.normal)),
                ),
          centerTitle: true,
          bottom: PreferredSize(
            preferredSize:
                Size.fromHeight((webviewController.isLoading.value) ? .5 : 0),
            child: (webviewController.isLoading.value)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: (cookieStr != null)
            ? (Platform.isAndroid
                ? WebAndroid(url: widget.url)
                : WebiOS(url: widget.url))
            : Container(
                width: getW(context),
                height: getH(context),
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(
                      //color: MyTheme.redColor,
                      ),
                ),
              ),
      ),
    );
  }
}
