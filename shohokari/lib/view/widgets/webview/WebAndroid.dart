import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/view/widgets/webview/WebBase.dart';
import 'package:aitl/view_model/generic/enum_gen.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';

class WebAndroid extends StatefulWidget {
  final String url;
  const WebAndroid({Key key, @required this.url}) : super(key: key);

  @override
  _WebAndroidState createState() => _WebAndroidState();
}

class _WebAndroidState extends WebBase<WebAndroid> with StateListener {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state, data) async {
    if (state == ObserverState.STATE_WEBVIEW_BACK) {
      flutterWebviewPlugin.goBack();
    }
  }

  onHideWebview() {
    flutterWebviewPlugin.hide();
  }

  onShowWebview() {
    flutterWebviewPlugin.show();
  }

  @override
  void initState() {
    super.initState();
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);

    flutterWebviewPlugin.onStateChanged.listen((viewState) {
      if (mounted) {
        if (viewState.type == WebViewState.startLoad) {
          webviewController.isLoading.value = true;
        } else if (viewState.type == WebViewState.finishLoad) {
          webviewController.isLoading.value = false;
        }
      }
    });
    flutterWebviewPlugin.onProgressChanged.listen((event) {
      webviewController.progress.value = (event * 100).toInt();
      print(webviewController.progress.value.toStringAsFixed(1));
    });
  }

  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    flutterWebviewPlugin.close();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      //resizeToAvoidBottomInset: false,
      resizeToAvoidBottomInset: true,
      scrollBar: true,
      /*appBar: AppBar(
        elevation: MyTheme.appbarElevation,
        backgroundColor: MyTheme.themeData.accentColor,
        automaticallyImplyLeading: false,
        title: FittedBox(
          fit: BoxFit.fitWidth,
          child: AutoSizeText((!isTermsBusiness) ? title : "Terms of Business",
              style: TextStyle(
                  color: MyTheme.redColor,
                  //fontSize: 17,
                  fontWeight: FontWeight.bold)),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(!isTermsBusiness ? Icons.arrow_back : Icons.close,
              color: MyTheme.redColor),
          onPressed: () async {
            if (!isTermsBusiness)
              Navigator.pop(context);
            else {
              flutterWebviewPlugin.goBack();
              isTermsBusiness = false;
              setState(() {});
            }
          },
        ),
      ),*/
      allowFileURLs: true,
      url: widget.url,
      //headers: {'Cookie': cookieStr},
      withLocalStorage: true,
      //enableAppScheme: true,
      hidden: true,
      withZoom: true,
      appCacheEnabled: false,
      debuggingEnabled: true,
      clearCookies: false,
      mediaPlaybackRequiresUserGesture: false,
      ignoreSSLErrors: true,
      //useWideViewPort: false,
      javascriptChannels: [
        JavascriptChannel(
            name: EnumGen.getEnum2Str(eHandlers.Back),
            onMessageReceived: (JavascriptMessage message) {
              onBack();
            }),
        JavascriptChannel(
          name: EnumGen.getEnum2Str(eHandlers
              .DownloadFileModule), //  remove HYPHEN in js handler and then check
          onMessageReceived: (JavascriptMessage message) async {
            if (message.message == null) return;
            if (message.message.length == 0) return;
            if (message.message != "") {
              print(message.message);
              onDownloadFileModule(message.message);
            }
          },
        ),
        JavascriptChannel(
            name: EnumGen.getEnum2Str(eHandlers.FullViewFileHandler),
            onMessageReceived: (JavascriptMessage message) {
              if (message.message == null) return;
              if (message.message.length == 0) return;
              if (message.message != "") {
                print(message.message);
                onFullViewFileHandler(message.message, (url) {
                  flutterWebviewPlugin.reloadUrl(url);
                });
              }
            }),

        /*JavascriptChannel(
            name: 'markidwidget-ident-button',
            onMessageReceived: (JavascriptMessage message) {
              print(message.message);
              return {'bar': 'bar_value', 'baz': 'baz_value'};
            }),*/
      ].toSet(),
      withJavascript: true,
      initialChild: Container(
        color: Colors.white,
        width: getW(context),
        height: getH(context),
        child: Center(
          child: CircularProgressIndicator(
              //color: MyTheme.redColor,
              ),
        ),
      ),
    );
  }
}
