import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;

enum eHandlers {
  Back,
  DownloadFileModule,
  FullViewFileHandler,
}

abstract class WebBase<T extends StatefulWidget> extends State<T> with Mixin {
  final webviewController = Get.put(WebviewController());

  onHideWebview();
  onShowWebview();

  onBack() {
    Navigator.of(context).pop();
  }

  onDownloadFileModule(String message) async {
    onHideWebview();
    await NetworkMgr.shared.downloadFile(
      context: context,
      url: message,
      callback: (f) {
        showToast(context: context, msg: "File downloaded successfully");
        Future.delayed(Duration(seconds: AppConfig.AlertDismisSec), () {
          onShowWebview();
        });
      },
    );
  }

  onFullViewFileHandler(String message, Function(String) callbackReload) {
    final extension = p.extension(message);
    print(extension);
    if (extension.toLowerCase() == ".pdf" ||
        extension.toLowerCase() == ".doc" ||
        extension.toLowerCase() == ".docx" ||
        extension.toLowerCase() == ".xls") {
      callbackReload(
          "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
              message);
    } else {
      onHideWebview();
      Get.to(() => PicFullView(
            url: message,
            title: "Document View",
          )).then((value) {
        onShowWebview();
      });
    }
  }

  onCallbackclientagreement() {
    //Get.offAll(MainCustomerScreen());
  }

  onCallbackclientrecommendationagreement() {
    //Get.offAll(MainCustomerScreen());
  }
}
