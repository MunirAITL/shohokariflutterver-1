import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/material.dart';

import 'MMArrowBtn.dart';

drawBottomBtn({
  @required context,
  @required String text,
  @required Function callback,
  Color bgColor,
  IconData icon,
}) {
  if (bgColor == null) {
    bgColor = Colors.white;
  }
  double height = MediaQuery.of(context).size.height;
  var padding = MediaQuery.of(context).padding;
  double newheight = height - padding.top - padding.bottom;
  return BottomAppBar(
    color: bgColor,
    child: Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
      child: MMArrowBtn(
        txt: text,
        icon: icon,
        height: newheight * MyTheme.btnHpa / 100,
        width: MediaQuery.of(context).size.width,
        callback: () {
          callback();
        },
      ),
    ),
    elevation: MyTheme.botbarElevation,
  );
}
