import 'dart:io';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class CamPicker with Mixin {
  List<String> _items = ['from_gallery'.tr, 'from_cam'.tr, 'skip'.tr];

  Future<File> _openCamera({isRear = true, String key}) async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.camera,
    ].request();
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      //preferredCameraDevice: (isRear) ? CameraDevice.rear : CameraDevice.front,
    );

    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  Future<File> _openGallery({String key}) async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.photos,
    ].request();
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  void showCamDialog(
      {BuildContext context, Function callback, bool isRear = true}) {
    showDialog<int>(
      context: context,
      builder: (context) =>
          AlertDialog(content: Text("choose_cam".tr), actions: [
        MaterialButton(
          child: Text("gallery".tr),
          onPressed: () => Navigator.pop(context, 0),
        ),
        MaterialButton(
          child: Text("camera".tr),
          onPressed: () => Navigator.pop(context, 1),
        ),
      ]),
    ).then((int choice) async {
      if (choice != null) {
        switch (choice) {
          case 0:
            callback(await _openGallery()); //  gallery
            break;
          case 1:
            callback(await _openCamera(isRear: isRear)); //  cam
            break;
          default:
            break;
        }
      }
    });
  }
}
