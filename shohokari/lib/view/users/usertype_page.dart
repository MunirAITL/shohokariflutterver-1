import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/RadioSelController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';

class UserTypePage2 extends StatelessWidget with Mixin {
  final radioSelController = new RadioSelController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: UIHelper().drawAppbarTitle(title: 'user_role'.tr),
          centerTitle: true,
        ),
        body: drawLayout(context),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(20),
          child: MMBtn(
              txt: "on_boarding_poster_four_continue_button_label".tr,
              height: getHP(context, MyTheme.btnHpa),
              width: getW(context),
              callback: () {
                Get.to(() => AuthScreen());
              }),
        ),
      ),
    );
  }

  drawLayout(context) {
    return Center(
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Txt(
                txt: "user_role_header".tr,
                txtColor: MyTheme.redColor,
                txtSize: MyTheme.txtSize + 1,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
              SizedBox(height: 20),
              drawRadioBox(
                  context: context,
                  index: 0,
                  title: "employer".tr,
                  subTitle: "user_role_employer_label".tr),
              SizedBox(height: 20),
              drawRadioBox(
                  context: context,
                  index: 1,
                  title: "worker".tr,
                  subTitle: "user_role_worker_label".tr),
            ],
          ),
        ),
      ),
    );
  }

  drawRadioBox(
      {BuildContext context, int index, String title, String subTitle}) {
    return Container(
      width: getWP(context, 50),
      //height: getWP(context, 50),
      //color: Colors.black,
      child: Card(
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 10),
              Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
              SizedBox(height: 10),
              Txt(
                txt: subTitle,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 10),
              Obx(() => Theme(
                    data: MyTheme.radioThemeData,
                    child: Radio(
                        value: index,
                        groupValue: radioSelController.radioIndex.value,
                        onChanged: (value) {
                          radioSelController.setSelectedIndex(
                              selectValue: value);
                        }),
                  )),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
