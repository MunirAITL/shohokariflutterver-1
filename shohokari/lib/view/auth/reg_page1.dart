import 'dart:io';

import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/textfield/InputBox.dart';
import 'package:aitl/view/widgets/textfield/InputBoxHT.dart';
import 'package:aitl/view/widgets/textfield/InputMobFlagBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/TCView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'auth_base.dart';
import 'reg_page2.dart';

class RegView extends StatefulWidget {
  @override
  State createState() => _RegViewState();
}

enum genderEnum { male, female }

class _RegViewState extends BaseAuth<RegView> with APIStateListener {
  //  reg1
  final fname = TextEditingController();
  final lname = TextEditingController();
  final email = TextEditingController();
  final pwd = TextEditingController();

  final mobile = TextEditingController();
  String countryDialCode = AppDefine.COUNTRY_DIALCODE;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(user: model.responseData.user);
              await userData.setUserModel();
            } catch (e) {
              log(e.toString());
            }
            if (!Server.isOtp) {
              Get.off(() => DashboardPage());
              //Get.off(() => RegView2());
            } else {
              Get.off(() => RegView2());
            }
          } else {
            try {
              if (mounted) {
                //final err =
                //model2.errorMessages.login[0].toString();
                showToast(
                  context: context,
                  msg: "You have entered invalid credentials",
                );
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.reg1 &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              //  recall login to get cookie
              APIViewModel().req<LoginAPIModel>(
                context: context,
                apiState: APIState(APIType.login, this.runtimeType, null),
                url: APIAuthCfg.LOGIN_URL,
                param: LoginHelper()
                    .getParam(email: email.text.trim(), pwd: pwd.text.trim()),
                reqType: ReqType.Post,
                isCookie: true,
              );
            } else {
              try {
                final err = model.errorMessages.register[0].toString();
                showToast(context: context, msg: err);
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      } else if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    fname.dispose();
    lname.dispose();
    mobile.dispose();
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  bool validate() {
    if (!UserProfileVal().isFNameOK(context, fname, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, lname, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, email, "email_invalid".tr, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, mobile, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, pwd, MyTheme.redColor)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 30),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 40, right: 40),
                child: Column(
                  children: [
                    InputBox(
                      ctrl: fname,
                      lableTxt: "create_profile_first_name_hint".tr,
                      kbType: TextInputType.name,
                      len: 20,
                      ecap: eCap.Word,
                    ),
                    SizedBox(height: 10),
                    InputBox(
                      ctrl: lname,
                      lableTxt: "create_profile_last_name_hint".tr,
                      kbType: TextInputType.name,
                      len: 20,
                      ecap: eCap.Word,
                    ),
                    SizedBox(height: 10),
                    InputBox(
                      ctrl: email,
                      lableTxt: "email_txt".tr,
                      kbType: TextInputType.emailAddress,
                      len: 50,
                    ),
                    SizedBox(height: 10),
                    InputMobFlagBox(
                      ctrl: mobile,
                      lableTxt: "mobile_number_label".tr,
                      len: 15,
                      getCountryCode: (code) {
                        countryDialCode = code.toString();
                        PrefMgr.shared.setPrefStr("countryName", code.code);
                        PrefMgr.shared
                            .setPrefStr("countryCode", code.toString());
                      },
                    ),
                    SizedBox(height: 20),
                    InputBox(
                      ctrl: pwd,
                      lableTxt: "password".tr,
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true,
                    ),
                    SizedBox(height: 10),
                    MMBtn(
                      txt: "sign_up_create_account".tr,
                      width: getW(context),
                      height: getHP(context, MyTheme.btnHpa),
                      radius: 10,
                      callback: () async {
                        if (validate()) {
                          APIViewModel().req<RegAPIModel>(
                            context: context,
                            apiState:
                                APIState(APIType.reg1, this.runtimeType, null),
                            url: APIAuthCfg.REG_URL,
                            reqType: ReqType.Post,
                            param: {
                              "Address": '',
                              "Agreement": true,
                              "BriefBio": "",
                              "Cohort": '',
                              "CommunityId": "",
                              "ConfirmPassword": pwd.text.trim(),
                              "DateofBirth": '',
                              "DeviceType":
                                  Platform.isAndroid ? 'Android' : 'iOS',
                              "Email": email.text.trim(),
                              "FirstName": fname.text.trim(),
                              "Headline": "",
                              "LastName": lname.text.trim(),
                              "Latitude": "0",
                              "LinkedUrl": "",
                              "Longitude": "0",
                              "MobileNumber":
                                  countryDialCode + ' ' + mobile.text.trim(),
                              "Password": pwd.text.trim(),
                              "ReferenceId": "",
                              "ReferenceType": "",
                              "ReferrerId": 0,
                              "Remarks": "",
                              "ReturnUrl": "",
                              "StanfordWorkplaceURL": "",
                              "TwitterUrl": "",
                              "Version": "28",
                            },
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Txt(
              txt: "sign_up_login_separator_or".tr,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    icon: "ic_fb",
                    txt: "com_facebook_loginview_log_in_button_continue".tr,
                    callback: () {
                      loginWithFB(this.runtimeType);
                    },
                  ),
                  SizedBox(height: 10),
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    icon: "ic_google",
                    txt: "com_google_loginview_log_in_button_continue".tr,
                    callback: () {
                      loginWithGoogle(this.runtimeType);
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
              child: Center(child: TCView(enum_tc: eTC.REG_TC)),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
