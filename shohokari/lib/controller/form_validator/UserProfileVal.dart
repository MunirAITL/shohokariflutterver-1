import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 8;

  isEmpty(context, TextEditingController tf, arg, Color bgColor) {
    if (tf.text.isEmpty) {
      showToast(context: context, msg: arg);
      return true;
    }
    return false;
  }

  isFullName(context, String name, Color bgColor) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showToast(context: context, msg: "fullname_invalid".tr);
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(context, TextEditingController tf, Color bgColor) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "fname_invalid".tr);
      return false;
    }
    return true;
  }

  isLNameOK(context, TextEditingController tf, Color bgColor) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "lname_invalid".tr);
      return false;
    }
    return true;
  }

  isEmailOK(context, TextEditingController tf, String msg, Color bgColor) {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+")
        .hasMatch(tf.text.trim())) {
      showToast(context: context, msg: msg);
      return false;
    } else if (tf.text.trim().contains("@help.mortgage-magic.co.uk")) {
      showToast(context: context, msg: "email_not_exists".tr);
      return false;
    }
    return true;
  }

  isPhoneOK(context, TextEditingController tf, Color bgColor) {
    if (tf.text.length < PHONE_LIMIT) {
      showToast(context: context, msg: "mobile_invalid".tr);
      return false;
    }
    return true;
  }

  isPwdOK(context, TextEditingController tf, Color bgColor) {
    if (tf.text.length < 3) {
      showToast(context: context, msg: "pwd_wrong".tr);
      return false;
    }
    return true;
  }

  isDOBOK(context, str, Color bgColor) {
    if (str == '') {
      showToast(context: context, msg: "dob_invalid".tr);
      return false;
    }
    return true;
  }
}
